<?php /*Template name: Продукция*/ ?>
<?php get_header(); ?>

<?php
	$division_name = 'Продукция';
	$division_url = '/category/produktsiya/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<main class="wrapper">
	<aside class="aside aside_product">
		<?php get_template_part('inc/product-menu'); ?>
	</aside>
	<div class="content content_product">
		<?php get_template_part('inc/image'); ?>
		<?php
			the_post();
			$content = apply_filters('the_content', get_the_content());

			if (strpos($content, '+table+') !== false) {
				$content = explode('+table+', $content);
				echo $content[0];
				?>
					<div class="product__table">
						<?php
							$id = get_the_ID();
							$table = get_post_meta($id, 'table', true) * 1;
							$table_title = get_post_meta($id, 'table_title', true);
							$table_content = get_post_meta($id, 'table_content', true);

							if (count($table_title) == count($table_content) && $table) {
								foreach ($table_title as $k => $v) {
									?>
										<div class="product__ceil">
											<div class="product__ceil-header">
												<?php echo $table_title[$k]; ?>
											</div>
											<div class="product__ceil-content">
												<?php echo $table_content[$k]; ?>
											</div>
										</div>
									<?php
								}
							}
						?>
					</div>
				<?php
				echo $content[1];
			} else {
				echo $content;
			}
		?>
		<br>
		<?php get_template_part('inc/single-form'); ?>
	</div>
</main>

<?php get_footer(); ?>