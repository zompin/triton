<?php
	$division_name = 'Продукция';
	$division_url = '/category/produktsiya/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<main class="wrapper">
	<aside class="aside aside_product">
		<?php get_template_part('inc/product-menu'); ?>
	</aside>	
	<div class="content content_product">
		<?php
			if (have_posts()) {
				?>
					<div class="previews-product-category preview-wide">
						<?php
							while (have_posts()) {
								the_post();
								get_template_part('inc/preview-product-category');
							}
						?>
					</div>
				<?php
			}
		?>
		<h1>СТРОИТЕЛЬСТВО С ИСПОЛЬЗОВАНИЕМ СЭНДВИЧ-ПАНЕЛЕЙ</h1>
		<p>Сэндвич-панели широко применяются для строительства различных зданий, в том числе, для возведения торговых и офисных помещений, сельскохозяйственных зданий, складских помещений, промышленных корпусов. Кроме того, сэндвич-панели широко используются при реконструкции фасадов и для утепления уже существующих зданий.</p>
		<h2>ХАРАКТЕРИСТИКИ СЭНДВИЧ-ПАНЕЛЕЙ</h2>
		<p>Сэндвич-панели – продукт, незаменимый в современном строительстве. Он обладает рядом несомненных преимуществ перед кирпичом, бетоном и другими строительными материалами. Основные достоинства сэндвич-панелей – это:</p>
		<ul>
			<li>Отличная теплоизоляция;</li>
			<li>Высокая огнестойкость;</li>
			<li>Высокая звукоизоляция;</li>
			<li>Воздухонепроницаемость и водонепроницаемость;</li>
			<li>Высокая несущая способность;</li>
			<li>Долговечность;</li>
			<li>Небольшой вес;</li>
			<li>Легкость сборки-разборки;</li>
			<li>Широкая цветовая гамма.</li>
		</ul>
		<p>За счет использования сэндвич-панелей можно в разы уменьшить толщину ограждающих конструкций и перегородок при строительстве зданий, что приводит к увеличению полезной площади здания и снижению нагрузок на каркас и фундаменты.</p>
		<p>При производстве сэндвич-панелей используется негорючая минеральная вата. Сэндвич-панели относятся к классу конструктивной пожарной опасности К0 (непожароопасные конструкции), что позволяет использовать их даже в качестве противопожарных преград.</p>
		<h2>ЦЕНЫ НА СЭНДВИЧ-ПАНЕЛИ</h2>
		<p>Чтобы рассчитать стоимость конкретного проекта, заполните заявку, указав основную информацию о проекте и свои контактные данные. Наши менеджеры помогут Вам рассчитать стоимость проекта и дадут квалифицированные советы по строительству зданий с использованием сэндвич-панелей.</p>
		<p>Компания «Triton» предлагает трехслойные сэндвич-панели, соответствующие всем международным показателям качества. Гарантийный срок эксплуатации продукции – 5 лет.</p>
		<p>В нашем ассортименте представлены сэндвич-панели 2 видов: кровельные и стеновые.</p>
		<h3>СТРУКТУРА СЭНДВИЧ-ПАНЕЛЕЙ</h3>
		<img src="<?php echo get_template_directory_uri() . '/imgs/senvich-panel-structure.png'; ?>">
		<p>Внешний слой сэндвич-панелей изготавливается из оцинкованного листа с полимерным покрытием и может быть окрашен в любой цвет по каталогу RAL. Внутренний металлический лист также покрыт полимером, защищающим его от коррозии. Между металлическими листами проложен утеплитель — негорючая минеральная (базальтовая) вата. Эти листы прочно соединены между собой двухкомпонентным полиуретановым клеем. Применяемые нами сэндвич-панели соответствуют всем требованиям пожарной безопасности, санитарным и экологическим нормам, предъявляемым к строительным материалам этого типа.</p>
		<p>Загибы на концах составляющих сэндвич-панель металлических листов образуют замковое соединение, усиленное саморезами, прочно соединяющее панели в единую конструкцию.</p>
	</div>
</main>