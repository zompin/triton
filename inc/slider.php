<div class="slider">
	<div class="slider__items">
		<?php
			$posts = get_posts(array('cat' => 5));

			$posts = array_reverse($posts);
			
			foreach ($posts as $post) {
				$post_title = $post->post_title;
				$post_content = $post->post_content;
				$post_image = get_post_meta($post->ID, 'image', true);
				$post_link = get_post_meta($post->ID, 'link', true);
				
				if (strpos($post_title, ',')) {
					$post_title = explode(',', $post_title);

					foreach ($post_title as $k => $v) {
						$post_title[$k] = '<div class="slider__item-title-div"><span>' . trim($v) . '</span></div>';
					}

					$post_title = implode('', $post_title);
				}
		?>
			<div class="slider__item">
				<?php if ($post_image): ?>
					<img src="<?php echo $post_image; ?>" class="slider__img">
				<?php endif; ?>
				<div class="slider__item-inner">
					<div class="slider__item-title">
						<?php echo $post_title; ?>
					</div>
					<div class="slider__item-desc">
						<?php echo $post_content; ?>
						<?php if ($post_link): ?>
							<a href="<?php echo $post_link; ?>" class="preview__more slider__item-link">Подробнее</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php
			}
		?>
	</div>
	<div class="slider__control"></div>
</div>