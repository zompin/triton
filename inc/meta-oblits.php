<?php
	$id = $post->ID;

	$oblits = get_post_meta($id, 'oblits', true);
	$oblits_name = get_post_meta($id, 'oblits_name', true);
	$oblits_image = get_post_meta($id, 'oblits_image', true);
?>

<div class="metabox__tab oblits">
	<input type="hidden" name="extra[oblits]" value="">
	<input type="checkbox" id="oblits-tab" name="extra[oblits]" value="1" <?php if ($oblits) echo 'checked'; ?>>
	<label for="oblits-tab">Виды облицовок</label>
	<div class="metabox__tab-inner">
		<?php
			if ($oblits_name && $oblits_image) {
				foreach ($oblits_name as $k => $v) {
					?>
						<div class="oblits__item">
							<label class="metabox__label">
								Название вида облицовки
								<textarea name="extra[oblits_name][]"><?php echo $oblits_name[$k]; ?></textarea>
							</label>
							<label class="metabox__label">
								Изображение вида облицовки
								<textarea name="extra[oblits_image][]"><?php echo $oblits_image[$k]; ?></textarea>
							</label>
							<button type="button" class="button button_remove-oblits">Удалить облицовку</button>
						</div>
					<?php
				}
			} else {
				?>
					<div class="oblits__item">
						<label class="metabox__label">
							Изображение вида облицовки
							<textarea name="extra[oblits_name][]"></textarea>
						</label>
						<label class="metabox__label">
							Название вида облицовки
							<textarea name="extra[oblits_image][]"></textarea>
						</label>
						<button type="button" class="button button_remove-oblits">Удалить облицовку</button>
					</div>
				<?php
			}
		?>
		<div class="button-cont">
			<button type="button" class="button button_add-oblits">Добавить облицовку</button>
		</div>
	</div>
</div>