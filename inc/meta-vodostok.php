<?php
	$id 			= $post->ID;
	$vodostok 		= get_post_meta($id, 'vodostok', true) * 1;
	$vodostok_elem 	= get_post_meta($id, 'vodostok_elem', true);
	$vodostok_ch 	= get_post_meta($id, 'vodostok_ch', true);
	$vodostok_value = get_post_meta($id, 'vodostok_value', true);
	$vodostok_image = get_post_meta($id, 'vodostok_image', true);
?>
<div class="metabox__tab">
	<input type="hidden" name="extra[vodostok]" value="">
	<input id="vodostok" type="checkbox" name="extra[vodostok]" value="1" <?php if ($vodostok) echo 'checked' ?>>
	<label for="vodostok">Элементы водостока</label>
	<div class="metabox__tab-inner vodostok">
		<?php
			if ($vodostok_elem && $vodostok_value && $vodostok_ch) {
				foreach ($vodostok_elem as $k => $v) {
					?>
						<div class="vodostok__elem">
							<label class="metabox__label">
								Элемент
								<textarea name="extra[vodostok_elem][]"><?php echo $vodostok_elem[$k]; ?></textarea>
							</label>
							<label class="metabox__label">
								Характеристика элементка
								<textarea name="extra[vodostok_ch][]"><?php echo $vodostok_ch[$k]; ?></textarea>
							</label>
							<label class="metabox__label">
								Назначение
								<textarea name="extra[vodostok_value][]"><?php echo $vodostok_value[$k]; ?></textarea>
							</label>
							<button type="button" class="button button_remove-vodostok">Удалить элемент</button>
						</div>
					<?php
				}
			} else {
				?>
					<div class="vodostok__elem">
						<label class="metabox__label">
							Элемент
							<textarea name="extra[vodostok_elem][]"></textarea>
						</label>
							<label class="metabox__label">
								Характеристика элементка
								<textarea name="extra[vodostok_ch][]"></textarea>
							</label>
						<label class="metabox__label">
							Назначение
							<textarea name="extra[vodostok_value][]"></textarea>
						</label>
						<button type="button" class="button button_remove-vodostok">Удалить элемент</button>
					</div>
				<?php
			}
		?>
		<div class="button-cont">
			<button type="button" class="button button_add-vodostok">Добавить элемент</button>
		</div>
		<label class="metabox__label">
			<textarea name="extra[vodostok_image]"><?php echo $vodostok_image; ?></textarea>
		</label>
	</div>
</div>