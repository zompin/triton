<?php /*Template name: Работа*/ ?>
<?php get_header(); ?>

<?php
	$division_name = 'Компания';
	$division_url = '/o-kompanii/';
?>

<?php get_template_part('inc/breadcrumbs'); ?>
<?php get_template_part('inc/image'); ?>

<main class="wrapper">

	<aside class="aside">
	<?php
		wp_nav_menu(
			array(
				'theme_location' => 'company',
				'fallback_cb' => '__return_empty_string',
				'depth' => 1,
				'container' => '',
				'menu_id' => '',
				'menu_class' => 'aside__menu'
			)
		);
	?>
	</aside>
	<div class="content content_company">
		<?php
			the_post();
			the_content();
		?>
		<button class="button-border button-border_fill">
			<span>ЗАПОЛНИТЬ РЕЗЮМЕ ОНЛАЙН</span>
		</button>
	</div>

</main>
<div class="wrapper wrapper-cv">
	<div class="wrapper-cv__inner">
		<div class="aside">&nbsp;</div>
			<div class="content">
				<div class="form_cv">
					<div class="form__text">Поля, отмеченные звездочкой (*), обязательны для заполнения</div>
					<div class="form__header-simple">Личные данные</div>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Фамилия: *</span>
						<input type="text" class="form__input form__input_cv form_require form__input_cv-lname">
					</label>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Имя: *</span>
						<input type="text" class="form_require form__input form__input_cv form__input_cv-fname">
					</label>
					<div class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Дата рождения: *</span>
						<span class="form__col form__col_date">
							<input type="text" class="form_require form__input form__input_cv-date form__input_cv-day" placeholder="ДД">
							<input type="text" class="form_require form__input form__input_cv-date form__input_cv-month" placeholder="ММ">
							<input type="text" class="form_require form__input form__input_cv-date form__input_cv-year" placeholder="ГГГГ">
						</span>
					</div>
					<div class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Адрес: *</span>
						<span class="form__col form__col_address">
							<input type="text" class="form_require form__input form__input_cv form__input_cv-area" placeholder="Область">
							<input type="text" class="form_require form__input form__input_cv form__input_cv-city" placeholder="Город">
							<input type="text" class="form_require form__input form__input_cv form__input_cv-street" placeholder="Улица">
							<input type="text" class="form_require form__input form__input_cv form__input_cv-home" placeholder="Дом">
							<input type="text" class="form_require form__input form__input_cv form__input_cv-q" placeholder="Кв.">
						</span>
					</div>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Телефон: *</span>
						<input type="text" class="form_require form__input form__input_cv form__input_cv-phone">
					</label>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">E-mail: *</span>
						<input type="text" class="form_require form__input form__input_cv form__input_cv-email">
					</label>
					<div class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv form__label-text_cv-top">Семейное положение:</span>
						<label class="form__radio">
							<input type="radio" name="status" checked="" class="form__input_cv-status" value="Холост / Не замужем">
							<span>Холост / Не замужем</span>
						</label>
						<label class="form__radio">
							<input type="radio" name="status" class="form__input_cv-status" value="Состою в браке">
							<span>Состою в браке</span>
						</label>
					</div>
					<div class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv form__label-text_cv-top">Дети:</span>
						<label class="form__radio">
							<input type="radio" name="kids" checked="" class="form__input_cv-kids" value="Нет">
							<span>Нет</span>
						</label>
						<label class="form__radio">
							<input type="radio" name="kids" class="form__input_cv-kids" value="Есть">
							<span>Есть</span>
						</label>
					</div>
					<div class="form__header-simple">Образование</div>
					<div class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv form__label-text_cv-top">Вид образования: *</span>
						<div>
							<label class="form__radio">
								<input type="radio" name="education" checked="" class="form__input_cv-educ-type" value="Высшее">
								<span>Высшее</span>
							</label>
							<label class="form__radio">
								<input type="radio" name="education" class="form__input_cv-educ-type" value="Средне-специальное">
								<span>Средне-специальное</span>
							</label>
							<label class="form__radio">
								<input type="radio" name="education" class="form__input_cv-educ-type" value="Среднее">
								<span>Среднее</span>
							</label>
						</div>
					</div>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Учебное заведение: *</span>
						<input type="text" class="form_require form__input form__input_cv form__input_cv-univ">
					</label>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Специальность: *</span>
						<input type="text" class="form_require form__input form__input_cv form__input_cv-spec">
					</label>
					<div class="form__header-simple">Опыт работы</div>
					<div class="form__text">Опишите 2-4 Ваших места работы в обратном порядке, начиная с последнего.</div>
					<div class="form__header-simple">ПОСЛЕДНЕЕ МЕСТО РАБОТЫ:</div>
					<div class="form__expirience">
						<button class="form__expririence-remove">&times;</button>
						<label class="form__label form__label_cv">
							<span class="form__label-text form__label-text_cv">Период работы:</span>
							<input type="text" class="form__input form__input_cv form__input_cv-period" placeholder="____-____ гг">
						</label>
						<label class="form__label form__label_cv">
							<span class="form__label-text form__label-text_cv">Название компании:</span>
							<input type="text" class="form__input form__input_cv form__input_cv-company">
						</label>
						<label class="form__label form__label_cv">
							<span class="form__label-text form__label-text_cv">Местонахождение:</span>
							<input type="text" class="form__input form__input_cv form__input_cv-location">
						</label>
						<label class="form__label form__label_cv">
							<span class="form__label-text form__label-text_cv">Деятельность компании:</span>
							<input type="text" class="form__input form__input_cv form__input_cv-company-area">
						</label>
						<label class="form__label form__label_cv">
							<span class="form__label-text form__label-text_cv">Должность:</span>
							<input type="text" class="form__input form__input_cv form__input_cv-place">
						</label>
					</div>
					<div>
						<button class="form__add-cv">
							<span>Добавить место работы</span>
						</button>
					</div>
					<div class="form__header-simple">Дополнительная информация</div>
					<div class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv form__label-text_cv-top">Компьютерные навыки:</span>
						<div>
							<label class="form__radio">
								<input type="radio" name="comp" checked="" class="form__input_comp" value="Нет">
								<span>Нет</span>
							</label>
							<label class="form__radio">
								<input type="radio" name="comp" class="form__input_comp" value="Пользователь">
								<span>Пользователь</span>
							</label>
							<label class="form__radio">
								<input type="radio" name="comp" class="form__input_comp" value="IT специалист">
								<span>IT специалист</span>
							</label>
						</div>
					</div>
					<div class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Знание языков:</span>
						<div>
							<label class="form__checkbox">
								<input type="checkbox" class="form__input-lang-kz">
								<span>Қазақ</span>
							</label>
							<label class="form__checkbox">
								<input type="checkbox" class="form__input-lang-ru">
								<span>Русский</span>
							</label>
							<label class="form__checkbox">
								<input type="checkbox" class="form__input-lang-en">
								<span>English</span>
							</label>
							<label class="form__checkbox">
								<input type="checkbox" class="form__input-lang-other">
								<span>Другой:</span>
							</label>
							<input type="text" class="form__input form__input_lang-other-name">
						</div>
					</div>
					<div class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv form__label-text_cv-top">Водительские права:</span>
						<div>
							<label class="form__radio">
								<input type="radio" name="drive" checked="" value="Нет" class="form__input_drive">
								<span>Нет</span>
							</label>
							<label class="form__radio">
								<input type="radio" name="drive" value="Есть" class="form__input_drive">
								<span>Есть</span>
							</label>
						</div>
					</div>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Личные качества:</span>
						<input type="text" class="form__input form__input_cv form__input_cv-priv">
					</label>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Хобби:</span>
						<input type="text" class="form__input form__input_cv form__input_cv-hob">
					</label>
					<div class="form__header-simple">Пожелания к будущему месту работы</div>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Область деятельности: *</span>
						<input type="text" class="form_require form__input form__input_cv form__input_cv-cpec-area">
					</label>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Должность: *</span>
						<input type="text" class="form_require form__input form__input_cv form__input_cv-fplace">
					</label>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Желаемый уровень оплаты:</span>
						<input type="text" class="form_require form__input form__input_cv form__input_cv-pay" placeholder="тенге">
					</label>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Загрузить фото:</span>
						<div>
							<span class="form__file-photo">
								<input type="file" class="form__file-photo-file">
								<span>выбрать файл</span>
							</span>
							<span class="form__file-photo-text">
								Вы можете загрузить фото в формтах JPG, BMP, PNG.
							</span>
						</div>
					</label>
					<label class="form__label form__label_cv">
						<span class="form__label-text form__label-text_cv">Код на картинке: *</span>
						<div class="form__captcha-cont form__captcha-cont_cv">
							<div class="form__captcha form__captcha_cv">
								<img src="<?php echo get_template_directory_uri() . '/captcha/captcha.php'; ?>" class="form__captcha-img">
							</div>
							<input type="text" class="form_require form__input form__input_captcha form__input_captcha_cv">
						</div>
					</label>
					<div>
						<button class="button-border button-border_send button_send-cv">
							<span>ОТПРАВИТЬ РЕЗЮМЕ</span>
						</button>
					</div>
				</div>
			</div>
	</div>
</div>

<?php get_footer(); ?>