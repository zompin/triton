<?php get_header(); ?>

<?php
	$division_name = 'Информация';
	$division_url = '/category/poleznaya-informatsiya/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<?php 
	$img = get_template_directory_uri() . '/imgs/info-image.jpg';
	get_template_part('inc/image'); 
?>

<main class="wrapper">

	<aside class="aside">
		<?php
			wp_nav_menu(
				array(
					'theme_location' => 'information',
					'fallback_cb' => '__return_empty_string',
					'depth' => 1,
					'container' => '',
					'menu_id' => '',
					'menu_class' => 'aside__menu'
				)
			);
		?>
	</aside>
	<div class="previews-information-category">
		<?php
			if (have_posts()) {
				while (have_posts()) {
					the_post();
					get_template_part('inc/preview-information');
				}
			}
		?>
	</div>
	
</main>

<?php get_footer(); ?>