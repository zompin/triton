<?php get_header(); ?>
<?php
	global $division_name;
	global $division_url;
	
	$division_name = 'Продукция';
	$division_url = '/category/produktsiya/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<div class="wrapper wrapper-product-single">
	<aside class="aside aside_product">
		<?php get_template_part('inc/product-menu'); ?>
	</aside>
	<div class="content content_product">
		<div class="product__head">
			<div class="product__head-photo">
				<div class="product__head-thumb" style="background-image: url(<?php echo get_post_meta(get_the_ID(), 'thumb', true); ?>);"></div>
				<div class="product__head-photos">
					<div class="product__head-photos-item product__head-photos-item_current">
						<div><img src="<?php echo get_post_meta(get_the_ID(), 'thumb', true); ?>"></div>
					</div>
					<?php
						$product_image = get_post_meta(get_the_ID(), 'product_image', true);

						if ($product_image) {
							foreach ($product_image as $v) {
								if ($v) {
									?>
										<div class="product__head-photos-item">
											<div><img src="<?php echo $v; ?>"></div>
										</div>
									<?php
								}
							}
						}
					?>
				</div>
			</div>
			<div class="product__head-desc">
				<div class="product__head-header">
					<?php the_title(); ?>
				</div>
				<div class="product__head-text"><?php echo get_post_meta(get_the_ID(), 'part_desc', true); ?></div>
				<?php include 'inc/single-advantages.php'; ?>
				<?php include 'inc/single-icons.php'; ?>
			</div>
		</div>
		<?php
			$id = get_the_ID();
			$table = get_post_meta($id, 'table', true) * 1;
			$table_title = get_post_meta($id, 'table_title', true);
			$table_content = get_post_meta($id, 'table_content', true);

			if (count($table_title) == count($table_content) && $table) {
				?>
					<div class="product__table">
				<?php
				foreach ($table_title as $k => $v) {
					?>
						<div class="product__ceil">
							<div class="product__ceil-header"><?php echo $table_title[$k]; ?></div>
							<div class="product__ceil-content"><?php echo $table_content[$k]; ?></div>
						</div>
					<?php
				}
				?>
					</div>
				<?php
			}
		?>
		<?php
			$id = get_the_id();
			$before_tabs = get_post_meta($id, 'before_tabs', true);
			echo $before_tabs;
		?>
		<?php include 'inc/single-tabs.php'; ?>
	</div>
</div>
<div class="wrapper">
	<div class="aside">&nbsp;</div>
	<div class="content product-content content_product-form">
		<?php get_template_part('inc/single-form'); ?>
	</div>
</div>
<?php
	$id = get_the_ID();
	$projects = get_post_meta($id, 'projects', true);
	$projects_cat = get_post_meta($id, 'projects_cat', true);

	if ($projects && $projects_cat) {
		$projects_items = get_posts(array('category' => $projects_cat, 'numberposts' => 3));
		?>
			<div class="wrapper wrapper-product-projects">
				<div class="wrapper-product-projects__inner">
					<div class="aside">&nbsp;</div>
					<div class="content product__projects">
						<div class="product__projects-header">
							<?php
								$current_project_cat = get_category($projects_cat);
								if (get_category($projects_cat)->description) {
									echo get_category($projects_cat)->description;
								} else {
									echo get_category($projects_cat)->name;
								}
							?>
						</div>
						<div class="product__projects-items">
							<?php
								foreach ($projects_items as $projects_item) {
									$project_thumb = get_post_meta($projects_item->ID, 'thumb', true);
									echo '<div class="product__projects-item" style="background-image: url(' . $project_thumb . ');"><div></div></div>';
								}
							?>
						</div>
						<a href="<?php echo home_url('/category/proekty/'); ?>" class="link-border link-border_projects">Перейти в галерею проектов</a>
					</div>
				</div>
			</div>
		<?php
	}
?>
<?php
	$komplekt = get_post_meta($id, 'komplekt', true);
	$komplekt_title = get_post_meta($id, 'komplekt_title', true);
	$komplekt_image = get_post_meta($id, 'komplekt_image', true);

	if ($komplekt):
?>
	<div class="wrapper">
		<div class="aside">&nbsp;</div>
		<div class="content product-content">
			<?php

				if ($komplekt && $komplekt_title && $komplekt_image):
			?>
				<div class="product__komplekt">
					<div class="product__komplekt-header">КОМПЛЕКТУЮЩИЕ</div>
					<div class="product__komplekt-inner">
						<div class="product__komplekt-items">
							<?php foreach ($komplekt_title as $k => $v): ?>
								<div class="product__komplekt-item">
									<div class="product__komplekt-item-image" style="background-image: url(<?php echo $komplekt_image[$k]; ?>);"></div>
									<div class="product__komplekt-item-title">
										<?php echo $komplekt_title[$k]; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="product__komplekt-controls">
							<button class="product__komplekt-button product__komplekt-button_prev"></button>
							<button class="product__komplekt-button product__komplekt-button_next"></button>
						</div>
					</div>
				</div>
			<?php endif;?>
		</div>
	</div>
<?php endif; ?>
<?php get_footer(); ?>