<?php
	wp_nav_menu(
		array(
			'theme_location' => 'product',
			'fallback_cb' => '__return_empty_string',
			'depth' => 2,
			'container' => '',
			'menu_id' => '',
			'menu_class' => 'aside__menu',
			'link_before' => '<button></button>'
		)
	);
?>