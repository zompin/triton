<?php /*Template name: Вопрос-Ответ*/ ?>
<?php get_header(); ?>
<?php $div_title = 'Инофрмация'; ?>

<?php
	$division_name = 'Информация';
	$division_url = '/category/poleznaya-informatsiya/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<?php get_template_part('inc/image'); ?>

<main class="wrapper">

	<aside class="aside">
		<?php
			wp_nav_menu(
				array(
					'theme_location' => 'information',
					'fallback_cb' => '__return_empty_string',
					'depth' => 1,
					'container' => '',
					'menu_id' => '',
					'menu_class' => 'aside__menu'
				)
			);
		?>
	</aside>
	<div class="content content_question">
		<?php
			the_post();
			the_content();
		?>
		<button class="button-border button-border_question button-border_question-show-form">
			<span>ЗАДАТЬ ВОПРОС СПЕЦИАЛИСТУ</span>
		</button>
	</div>
</main>
<div class="wrapper-question wrapper-question_form">
	<div class="wrapper-question__inner">
		<aside class="aside"></aside>
		<div class="form_question">
			<label class="form__label form__label_wide">
				<span class="form__label-text form__label-text_require">Фамилия Имя:</span>
				<input type="text" class="form__input form__input_question form_require form__input_question-name">
			</label>
			<label class="form__label form__label_wide">
				<span class="form__label-text form__label-text_require">E-mail:</span>
				<input type="text" class="form__input form__input_question form_require form__input_question-email">
			</label>
			<label class="form__label form__label_wide">
				<span class="form__label-text">Телефон:</span>
				<input type="text" class="form__input form__input_question form__input_question-phone" placeholder="+7(___)___-__-__">
			</label>
			<label class="form__label form__label_wide">
				<span class="form__label-text form__label-text_require">Вопрос:</span>
				<textarea class="form__textarea form__textarea_question form_require  form__input_question-question"></textarea>
			</label>
			<label class="form__label form__label_wide form__label_captcha">
				<span class="form__label-text form__label-text_captcha form__label-text_require">Код на картинке:</span>
				<div class="form__captcha form__captcha_question">
					<img src="<?php echo get_template_directory_uri() . '/captcha/captcha.php' ?>" class="form__captcha-img">
				</div>
				<input type="text" class="form__input form__input_captcha form__input_captcha-question form_require form__input_question-captcha">
			</label>
			<div>
				<div>
					<button class="button-border button-border_question button-border_send button-bordered_question">
						<span>ОТПРАВИТЬ ВОПРОС</span>
					</button>
				</div>
				<span class="form__label-text form__label-text_require"></span>
				<span class="form__text"> - поля обязательные для заполнения</span>
			</div>
		</div>
	</div>
</div>
<div class="wrapper-question wrapper-question_questions">
	<div class="wrapper-question__inner">
		<aside class="aside">&nbsp;</aside>
		<div class="questions">
			<h3 class="questions__header">ВЫБРАТЬ ПРАВИЛЬНЫЙ ПРОДУКТ</h3>
			<a href="#" class="questions__all arrow-after" data-hide="Свернуть все" data-show="Развернуть все" data-state="show">Развернуть все</a>
			<div class="question__questions">
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Какой цвет кровли выбрать?</a>
					<div class="questions__item-desc">
						<p>Выбор цвета, как правило, зависит только от вас.</p>
						<p>Мы предлагаем широкий ассортимент металлочерепицы, профлистов и металлического сайдинга; полную палитру цветов кровельных материалов вы можете найти на нашем сайте.</p>
						<p>Чтобы посмотреть, как сочетаются ваши любимые оттенки с цветовой гаммой и стилем дома, можно воспользоваться специализированной программой Dream House.</p>
						<p>Вы также можете обсудить цвета кровли с одним из наших менеджеров в офисе продаж адреса вы найдете в разделе – Контакты.</p>
					</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">В какое время года лучше всего начать установку кровли?</a>
					<div class="questions__item-desc">
						<p>Большая часть монтажных работ производится летом, соответственно, лето – это время максимальной загрузки проектами. Поэтому могут возникнуть сложности при согласовании вашего графика с графиком подрядчика по монтажу.</p>
						<p>Однако, не существует каких-либо технических ограничений на установку металлической кровли в любое другое время года. Поэтому вы можете запланировать установку кровли на подходящее для вас время.</p>
					</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Необходимо ли использовать слой гидроизоляции?</a>
					<div class="questions__item-desc">
						<p>Гидроизоляция кровли – необходимая мера, которая выполняет важную функцию: предохраняет стропильную систему от разрушительного влияния атмосферной влаги и различных водных растворов. Если не проложить слой гидроизоляции во время монтажа крыши, то атмосферные осадки могут спровоцировать быстрый износ деревянного основания крыши, что сократит срок службы всей кровли. </p>
					</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Сколько лет может прослужить кровля из металлочерепицы?</a>
					<div class="questions__item-desc">Металлочерепица представляет собой металлический оцинкованный лист с полимерным покрытием, увеличивающий срок службы этого кровельного материала и защищающий его от негативного воздействия окружающей среды. Срок службы металлочерепицы составляет от 20 до 50 лет.</div>
				</div>
			</div>
			<h3 class="questions__header">СОВЕТЫ ПО УСТАНОВКЕ КРОВЛИ</h3>
			<a href="#" class="questions__all arrow-after" data-hide="Свернуть все" data-show="Развернуть все" data-state="show">Развернуть все</a>
			<div class="question__questions">
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Сколько времени обычно занимает установка крыши?</a>
					<div class="questions__item-desc">Установка крыши обычно занимает от двух дней до нескольких недель, в зависимости от масштаба проекта. Хотя крыши можно устанавливать практически в любое время года, погодные условия, такие как ветер, снег или дождь, могут повлиять на сроки выполнения проекта.</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">После доставки кровли должен ли я организовать её хранение?</a>
					<div class="questions__item-desc">Если вы заказали доставку кровельного комплекта на свой объект, вам необходимо обеспечить достаточно места для хранения материалов до начала установки кровли.</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Каков порядок настила кровли?</a>
					<div class="questions__item-desc">Практически любой кровельный материал укладывается снизу вверх, чтобы верхний лист перекрывал нижний. Монтаж кровли начинается с левого нижнего угла. Чтобы получить ровную линию карниза, необходимо обратить внимание на укладку первого листа.</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Как подготовить кровлю под монтаж металлочерепицы?</a>
					<div class="questions__item-desc">Вначале убедитесь, что поверхность кровли ровная и при необходимости исправьте дефекты. Не поленитесь проверить правильность формы и размеров крыши, для чего измерьте диагонали скатов, которые должны быть равны. Если это не так, значит есть перекос крыши. В случае, если перекос исправить уже никак нельзя, надо укладывать металлочерепицу так, чтобы нижний край обрешетки совпадал с линией свеса листов кровли.</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Как правильно выполнить обрешетку для укладки кровли из металлочерепицы?</a>
					<div class="questions__item-desc">В процессе подготовки монтажа металлочерепицы очень важно правильно выполнить обрешетку. Используются доски шириной примерно 100 мм. Толщина выбирается проектировщиками кровли. Выходящая на карниз доска должна быть толще на 10-15 мм. Расстояние между досками обрешетки равняется поперечному шагу профиля металлочерепицы – 350 мм или 400 мм. Расстояние между доской, выходящей на карниз, и последующей на 50 мм меньше (300 мм или 350 мм).</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Нужна ли при устройстве кровли из металлочерепицы контробрешетка?</a>
					<div class="questions__item-desc">При устройстве любой металлической кровли с использованием теплоизоляционного материала, кроме обрешетки, необходимо сделать контробрешетку. В этом случае каждый слой кровельного материала будет проветриваться, что повысит качество крыши.</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Как крепятся карнизные планки при устройстве кровли из металлочерепицы?</a>
					<div class="questions__item-desc">Карнизные планки крепятся к последней доске обрешетки. Нахлёст планок по длине – 100 мм. После крепления карнизных планок можно приступать к монтажу листов металлочерепицы.</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Каково значение необходимого уклона водосточного желоба на 1 метре?</a>
					<div class="questions__item-desc">Рекомендуем устанавливать водосточные желоба с уклоном приблизительно 2-3 мм на 1 метр.</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Сколько водосточных труб должно быть подведено к желобам?</a>
					<div class="questions__item-desc">Если поверхность ската крыши меньше 10 кв. метров, то можно установить одну водосточную трубу и подвести к ней несколько желобов с соответствующим уклоном. Если поверхность ската крыши более 10 кв. метров, то следует установить две или более водосточные трубы и обеспечить уклон желобов в направлении этих труб.</div>
				</div>
			</div>
			<h3 class="questions__header">СОВЕТЫ ПО МОНТАЖУ МЕТАЛЛОСАЙДИНГА</h3>
			<a href="#" class="questions__all arrow-after" data-hide="Свернуть все" data-show="Развернуть все" data-state="show">Развернуть все</a>
			<div class="question__questions">
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">В какие сроки выполняются работы по монтажу?</a>
					<div class="questions__item-desc">Как правило, бригада состоит из 2-3 человек. Дом площадью 100 кв.м монтируется ими в течение 5-7 дней.</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">Дом облицован вагонкой. Надо ли снимать вагонку, для того, чтобы покрыть дом металлосайдингом?</a>
					<div class="questions__item-desc">Нет, вагонку снимать нет необходимости. Вполне допустимо крепить металлосайдинг непосредственно к поверхности дома, облицованного деревянной вагонкой, при условии, что стена и углы ровные. Если же стена неровная, а это абсолютное большинство случаев, на деревянную стену устанавливается выравнивающая обрешетка.</div>
				</div>
				<div class="questions__item">
					<a href="#" class="questions__item-header arrow-before">А под металлосайдинг пароизоляцию кладут?</a>
					<div class="questions__item-desc">Пароизоляция необходима для правильной эксплуатации (задержка влажного воздуха) составляющих конструкцию стены: сруб, теплоизоляция. Металлосайдинг пароизоляцию не заменяет. Он необходим для защиты от внешних условий. Поэтому – нужно ли класть пароизоляцию под металлосайдинг – вопрос с самим металлосайдингом не связанный.</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>