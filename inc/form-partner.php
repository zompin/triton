<div class="form__fixed form__fixed_partner">
	<div class="form__close-div"></div>
	<div class="form__cont">
		<div class="form form_partner">
			<button class="form__close">&times;</button>
			<div class="form__header">&nbsp;</div>
			<label class="form__label">
				<span class="form__label-text form__label-text_partner form__label-text_require">Фамилия Имя</span>
				<input type="text" class="form__input form__input_partner form__input_partner-name form_require">
			</label>
			<label class="form__label">
				<span class="form__label-text form__label-text_partner form__label-text_require">Ваш e-mail или номер телефона</span>
				<input type="text" class="form__input form__input_partner form__input_partner-phone form_require">
			</label>
			<label class="form__label">
				<span class="form__label-text form__label-text_require">Сообщение</span>
				<textarea class="form__textarea form__textarea_partner form__input_partner-message form_require"></textarea>
			</label>
			<div class="form__captcha">
				<img src="<?php echo get_template_directory_uri() . '/captcha/captcha.php' ?>" class="form__captcha-img">
			</div>
			<label class="form__label form__label_after-captcha">
				<span class="form__label-text form__label-text_partner form__label-text_require">Код на картинке</span>
				<input type="text" class="form__input form__input_partner form__input_partner-captcha form_require">
			</label>
			<div class="form__text">Введите символы, которые показаны на картинке</div>
			<button class="form__button form__button_partner">ОТПРАВИТЬ</button>
		</div>
	</div>
</div>