	<footer class="footer">
		<div class="footer__black">
			<div class="footer__black-inner">
				<div class="footer__item">
					<div class="footer__item-header">Офисы продаж</div>
					<div class="footer__contact">
						<div class="footer__contact-city">Караганда:</div>
						<div class="footer__contact-item">Бульвар Мира, 30</div>
						<div class="footer__contact-item">Отдел розничных продаж:</div>
						<div class="footer__contact-item">Тел.:   <a href="tel:+77212566235" class="link-no-decorate">+7 /7212/ 566 235</a></div>
						<div class="footer__contact-item">Факс: <a href="tel:+77212566504" class="link-no-decorate">+7 /7212/ 566 504</a></div>
						<div class="footer__contact-item">E-mail: <a href="mailto:sales@trt.kz" class="link-no-decorate">sales@trt.kz</a></div>
					</div>
					<div class="footer__contact">
						<div class="footer__contact-city">Астана:</div>
						<div class="footer__contact-item">Шоссе Алаш, 34/1, </div>
						<div class="footer__contact-item">ТСК «BIG-ШАНХАЙ», офис №3</div>
						<div class="footer__contact-item">Тел.: <a href="tel:+77172493268" class="link-no-decorate">+ 7 /7172/ 493 268</a></div>
						<div class="footer__contact-item">Сот.: <a href="tel:+77012555957" class="link-no-decorate">+7 701 255 59 57</a></div>
						<div class="footer__contact-item">E-mail: <a href="mailto:astana-tritonm@mail.ru" class="link-no-decorate">astana-tritonm@mail.ru</a></div>
					</div>
					<div class="footer__contact">
						<div class="footer__item-header footer__item-header_prod">Производство</div>
						<div class="footer__contact-item">п. Уштобе (с/х им. Энгельса),</div>
						<div class="footer__contact-item">ул. Шоссейная, 2 А</div>
						<div class="footer__contact-item">Отдел оптовых продаж:</div>
						<div class="footer__contact-item">Тел.: <a href="tel:+77215429219" class="link-no-decorate">+7 /72154/ 29 219</a></div>
						<div class="footer__contact-item"><a href="tel:+77215429307" class="link-no-decorate">+7 /72154/ 29 307</a></div>
						<div class="footer__contact-item">E-mail: <a href="mailto:opt_sales@trt.kz" class="link-no-decorate">opt_sales@trt.kz</a></div>
					</div>
				</div>
				<div class="footer__item footer__item_product">
					<div class="footer__item-header">Каталог продукции</div>
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'product',
								'fallback_cb' => '__return_empty_string',
								'depth' => 1,
								'container' => '',
								'menu_id' => '',
								'menu_class' => 'footer__menu'
							)
						);
					?>
				</div>
				<div class="footer__item footer__item_news">
					<?php $posts = get_posts(array('category' => 6, 'numberposts' => 4)); ?>
					<?php if($posts): ?>
						<div class="footer__item-header">Последние новости</div>
						<div class="footer__news">
							<?php

								foreach ($posts as $post) {
									$id = $post->ID;
									?>
										<a href="<?php the_permalink($id); ?>" class="footer__news-item">
											<img src="<?php echo get_post_meta($id, 'thumb', true); ?>">
										</a>
									<?php
								}
							?>
						</div>
					<?php endif; ?>
					<div class="footer__item-header">Подписка на новости</div>
					<form class="footer__subscribe">
						<input type="text" class="footer__subscribe-input" placeholder="E-mail">
						<button type="submit" class="button-red footer__subscribe-submit">ПОДПИСАТЬСЯ</button>
					</form>
				</div>
			</div>
		</div>
		<div class="footer__red">
			<div class="footer__red-inner">
				<div class="footer__copy">2013 - <?php echo date('Y'); ?> &copy; ТОО «ТРИТОН М»  Все права защищены.</div>
				<a href="http://pr-d.kz/" target="_blank" class="footer__dev"  rel="nofollow">
					<img src="<?php echo get_template_directory_uri() . '/imgs/logo-dev.png'; ?>">
				</a>
			</div>
		</div>
	</footer>
</div>
<button class="to-top">
	<span></span>
</button>
<div class="popup">
	<button class="popup__close">&times;</button>
	<div class="popup__inner">
		<img class="popup__img">
	</div>
</div>
<div id="price">
	<?php
		$id = url_to_postid('/prais');
		$price = get_post($id);

		if ($id) {
			if ($price) {
				$price = strip_tags($price->post_content, '<a>');
				echo $price;
			}
		}

	?>
</div>
<?php get_template_part('inc/form-buttons'); ?>
<?php get_template_part('inc/form-price'); ?>
<?php get_template_part('inc/form-partner'); ?>
<?php get_template_part('inc/form-calc'); ?>
<?php wp_footer(); ?>
<!--
	Алтухов Илья
	zompin@ya.ru
-->
<script type="text/javascript" src="//cdn.callbackhunter.com/cbh.js?hunter_code=06256219b83940281183dd90bf87ca0b" charset="UTF-8"></script>
</body>
</html>