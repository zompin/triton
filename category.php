<?php get_header(); ?>

<?php
	$current_category = get_category( get_query_var( 'cat' ));
	$parent_category = get_category($current_category->parent);

	if ($parent_category->slug == 'metallocherepitsa' || $current_category->slug == 'metallocherepitsa') {
		include 'category--metallocherepitsa.php';
	} elseif ($parent_category->slug == 'proflist' || $current_category->slug == 'proflist') {
		include 'category--proflist.php';
	} elseif ($parent_category->slug == 'metallosaiding' || $current_category->slug == 'metallosaiding') {
		include 'category--metallosaiding.php';
	} elseif ($parent_category->slug == 'evroshtaketnik' || $current_category->slug == 'evroshtaketnik') {
		include 'category--evroshtaketnik.php';
	} elseif ($parent_category->slug == 'sendvich-paneli' || $current_category->slug == 'sendvich-paneli') {
		include 'category--sendvich-paneli.php';
	}
?>

<?php get_footer(); ?>