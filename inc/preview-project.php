<?php $content = strip_tags($post->post_content); ?>
<div class="preview preview_project">
	<a href="<?php echo home_url('/category/proekty/'); ?>" class="preview__inner preview__inner_project-category" style="background-image: url(<?php echo get_post_meta($post->ID, 'thumb', true); ?>);"></a href="<?php echo home_url('/category/proekty/'); ?>">
	<div class="preview__desc">
		<h2 class="preview__title">
			<?php the_title(); ?>
		</h2>
		<?php if ($content): ?>
			<p class="preview__about">
				<?php echo $content; ?>
			</p>
		<?php endif; ?>
		<a href="<?php echo home_url('/category/proekty/'); ?>" class="preview__more">Подробнее</a>
	</div>
</div>