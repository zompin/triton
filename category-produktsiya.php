<?php get_header(); ?>
<?php
	$division_name = 'Продукция';
?>
<?php get_template_part('inc/breadcrumbs'); ?>
<div class="wrapper">
	<aside class="aside aside_product">
		<?php get_template_part('inc/product-menu'); ?>
	</aside>
	<div class="content content_product">
		<div class="previews-product-main preview-product-main">
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/metallocherepitsa/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/cherepitsa.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">МЕТАЛЛОЧЕРЕПИЦА</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/proflist/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/proflist.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">ПРОФЛИСТ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/metallosaiding/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/saiding.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">МЕТАЛЛОСАЙДИНГ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/vodostok-modern/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/vod-sistemy.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">ВОДОСТОЧНЫЕ  СИСТЕМЫ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/evroshtaketnik/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/evrostak.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">Штакетник</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/profil-stroitelnyy/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/profil.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">ПРОФИЛЬ СТРОИТЕЛЬНЫЙ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/sendvich-paneli/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/sendvich-paneli.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">СЭНДВИЧ-ПАНЕЛИ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/stal-listovaya/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/stal.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">СТАЛЬ  ЛИСТОВАЯ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/shtrips-2/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/shtrips.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">ШТРИПС</div>
				</a>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>