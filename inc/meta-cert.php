<?php
	$id 		= $post->ID;
	$cert_img 	= get_post_meta($id, 'cert_img', true);
	$cert_name 	= get_post_meta($id, 'cert_name', true);
	$certs 		= get_post_meta($id, 'certs', true) * 1;
?>

<div class="metabox__tab">
	<input type="hidden" name="extra[certs]" value="">
	<input type="checkbox" id="cert-tab" name="extra[certs]" value="1" <?php if ($certs) echo 'checked' ?>>
	<label for="cert-tab">Сертификаты (для раздела "О компании")</label>
	<div class="metabox__tab-inner cert">
		<?php
			if ($cert_img && $cert_name) {
				foreach ($cert_name as $k => $v) {
					?>
						<div class="cert__item">
							<label class="metabox__label">
								Название сертификата
								<textarea name="extra[cert_name][]"><?php echo $cert_name[$k]; ?></textarea>
							</label>
							<label class="metabox__label">
								Изображение сертификата
								<textarea name="extra[cert_img][]"><?php echo $cert_img[$k]; ?></textarea>
							</label>
							<button type="button" class="button button_cert-remove">Удалить сертификат</button>
						</div>
					<?php
				}
			} else {
				?>
					<div class="cert__item">
						<label class="metabox__label">
							Название сертификата
							<textarea name="extra[cert_name][]"></textarea>
						</label>
						<label class="metabox__label">
							Изображение сертификата
							<textarea name="extra[cert_img][]"></textarea>
						</label>
						<button type="button" class="button button_cert-remove">Удалить сертификат</button>
					</div>
				<?php
			}
		?>
		<div class="cert__button-cont">
			<button type="button" class="button button_add-cert">Добавить сертификат</button>
		</div>
	</div>
</div>