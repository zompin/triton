<?php $content = strip_tags($post->post_content); ?>
<div class="preview">
	<div class="preview__inner preview__inner_project-category" style="background-image: url(<?php echo get_post_meta($post->ID, 'thumb', true); ?>);">
		<?php
			if (is_super_admin()) {
				echo '<a class="preview__edit" href="/wp-admin/post.php?post=' . get_the_ID() . '&action=edit"></a>';
			}
		?>
	</div>
	<div class="preview__desc">
		<h2 class="preview__title">
			<?php the_title(); ?>
		</h2>
		<?php if ($content): ?>
			<p class="preview__about">
				<?php echo $content; ?>
			</p>
		<?php endif; ?>
	</div>
</div>