<?php
	$id = $post->ID;
	$product_image = get_post_meta($id, 'product_image', true);
?>

<div class="metabox__tab">
	Изображения продукта
	<div class="product-photo">
		<?php
			if ($product_image) {
				foreach ($product_image as $v) {
					?>
						<div class="product-photo__photo">
							<textarea name="extra[product_image][]"><?php echo $v; ?></textarea>
							<button class="button button_remove-product-photo">Удалить изображение</button>
						</div>
					<?php
				}
			} else {
				?>
					<div class="product-photo__photo">
						<textarea name="extra[product_image][]"></textarea>
						<button class="button button_remove-product-photo">Удалить изображение</button>
					</div>
				<?php
			}
		?>
		<div class="button-cont">
			<button type="button" class="button button_add-product-photo">Добавить изображение</button>
		</div>
	</div>
</div>