<?php
	$division_name = 'Продукция';
	$division_url = '/category/produktsiya/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<main class="wrapper">
	<aside class="aside aside_product">
		<?php get_template_part('inc/product-menu'); ?>
	</aside>	
	<div class="content content_product">
		<?php
			if (have_posts()) {
				?>
					<div class="previews-product-category preview-wide">
						<?php
							while (have_posts()) {
								the_post();
								get_template_part('inc/preview-product-category');
							}
						?>
					</div>
				<?php
			}
		?>
		<h1>ШТАКЕТНИК – СОВРЕМЕННОЕ РЕШЕНИЕ ЗАБОРНОГО ОГРАЖДЕНИЯ</h1>
		<p>Металлический штакетник – новинка в оформлении домов и загородных участков. Это профиль, изготавливающийся на специальных профилирующих линиях из оцинкованного металла толщиной 0,5 мм с полимерным защитно-декоративным покрытием с широким выбором размеров, цветов и фактур. </p>
		<p>Металлический штакетник призван заменить традиционный забор, поскольку не нуждается в обслуживании и выглядит более привлекательно. Штакетник пригоден для создания не только заборов вокруг участка, но и ограждений для клумб и газонов. Ограждение из штакетника лёгкое и прозрачное, с ним ваш участок станет светлым и открытым, но вместе с тем защищённым. По своим характеристикам прочности и жесткости металлический штакетник соответствует забору из дерева или профнастила. Защитное полимерное и цинковое покрытие предотвращает появление ржавчины. Простое крепление саморезами или на заклёпки позволит установить забор быстро и легко даже непрофессионалу. В качестве каркаса, как правило, используются профилированные трубы различного сечения.</p>
		<p>Компания «Triton» производит 2 вида штакетника: П-образный и М-образный, различной длины и цветовой гаммы с декоративным резом верхней кромки. Цена штакетника выгодней стоимости деревянного забора.</p>
		<h2>ПРЕИМУЩЕСТВА ШТАКЕТНИКА</h2>
		<h3 class="icon icon_inf">ДОЛГОВЕЧНОСТЬ</h3>
		<p>Ваш красивый забор из металлического штакетника прослужит максимально долго и сохранит свой привлекательный внешний вид. Этого удалось добиться благодаря использованию тонколистового проката достаточной толщины с большим слоем цинка и современных полимерных покрытий.</p>
		<h3 class="icon icon_simple">ПРОСТОТА МОНТАЖНЫХ РАБОТ</h3>
		<p>Вы легко самостоятельно сможете смонтировать ограждение для дачи или коттеджа. Монтаж невероятно прост – из инструментов вам понадобится практически только один шуруповёрт. Благодаря самостоятельному монтажу, вы существенно сэкономите на общей стоимости готового ограждения.</p>
		<h3 class="icon icon_delivery1">УДОБСТВО ТРАНСПОРТИРОВКИ И ХРАНЕНИЯ</h3>
		<p>Вам будет просто найти место, где расположить штакетник при транспортировке и хранении. Штакетник для забора упаковывается стопками, ширина которых невелика, в отличии, например от ширины поддонов с профнастилом. Поэтому для транспортировки и хранения вам понадобится гораздо меньше места.</p>
		<h3 class="icon icon_exp">УДОБСТВО В ЭКСПЛУАТАЦИИ</h3>
		<p>Благодаря качественным полимерным покрытиям, ваше ограждение останется в первозданном виде на многие годы. В отличие от деревянного забора, металлический штакетник не потребует от вас периодической подкраски.</p>
		<h3 class="icon icon_univ1">УНИВЕРСАЛЬНОСТЬ</h3>
		<p>Штакетник позволяет оформить периметр вашего дома с обеих сторон. Для этого вы можете выбрать двустороннее полимерное покрытие или закрепить штакетник и с лицевой, и с внутренней стороны в шахматном порядке или напротив друг друга. В любом случае, ограждение будет выглядеть красиво со всех сторон.</p>
	</div>
</main>