<?php
	if (in_category('novosti')) {
		get_template_part('single-novosti');
	} elseif(in_category('poleznaya-informatsiya')) {
		get_template_part('single-information');
	} elseif (in_parent_category('produktsiya', get_the_ID()) || in_category(1)) {
		get_template_part('single-product');
	} else {
		get_template_part('single-default');
	}
?>