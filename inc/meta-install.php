<?php
	$id = $post->ID;

	$install = get_post_meta($id, 'install', true);
	$install_text = get_post_meta($id, 'install_text', true);
	$install_name = get_post_meta($id, 'install_name', true);
	$install_image = get_post_meta($id, 'install_image', true);
	$install_desc = get_post_meta($id, 'install_desc', true);
?>

<div class="metabox__tab install">
	<input type="hidden" name="extra[install]" value="">
	<input type="checkbox" id="install-tab" name="extra[install]" value="1" <?php if ($install) echo 'checked'; ?>>
	<label for="install-tab">Монтаж</label>
	<div class="metabox__tab-inner">
		<label class="label__metabox">
			Текст в заголовке
			<textarea name="extra[install_text]"><?php echo $install_text; ?></textarea>
		</label>
		<?php
			if ($install_name && $install_image) {
				foreach ($install_name as $k => $v) {
					?>
						<div class="install__item">
							<label class="metabox__label">
								Заголовок монтажа
								<textarea name="extra[install_name][]"><?php echo $install_name[$k]; ?></textarea>
							</label>
							<label class="metabox__label">
								Изображение монтажа
								<textarea name="extra[install_image][]"><?php echo $install_image[$k]; ?></textarea>
							</label>
							<label class="metabox__label">
								Описание монтажа
								<textarea name="extra[install_desc][]"><?php echo $install_desc[$k]; ?></textarea>
							</label>
							<button type="button" class="button button_remove-install">Удалить монтаж</button>
						</div>
					<?php
				}
			} else {
				?>
					<div class="install__item">
						<label class="metabox__label">
							Заголовок монтажа
							<textarea name="extra[install_name][]"></textarea>
						</label>
						<label class="metabox__label">
							Изображение монтажа
							<textarea name="extra[install_image][]"></textarea>
						</label>
						<label class="metabox__label">
							Описание монтажа
							<textarea name="extra[install_desc][]"></textarea>
						</label>
						<button type="button" class="button button_remove-install">Удалить монтаж</button>
					</div>
				<?php
			}
		?>
		<div class="button-cont">
			<button type="button" class="button button_add-install">Добавить монтаж</button>
		</div>
	</div>
</div>