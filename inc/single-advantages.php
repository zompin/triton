<?php
	$id = get_the_ID();

	$adv 				= get_post_meta($id, 'adv', true);
	$adv_streight 		= get_post_meta($id, 'adv_streight', true);
	$adv_simple 		= get_post_meta($id, 'adv_simple', true);
	$adv_color 			= get_post_meta($id, 'adv_color', true);
	$adv_cor_streight 	= get_post_meta($id, 'adv_cor_streight', true);
	$adv_wallet 		= get_post_meta($id, 'adv_wallet', true);
	$adv_ten 			= get_post_meta($id, 'adv_ten', true);
	$adv_fifteen 		= get_post_meta($id, 'adv_fifteen', true);
	$adv_fire 			= get_post_meta($id, 'adv_fire', true);

	if ($adv) {
		?>
			<div class="product__head-advantages">
				<?php if ($adv_ten): ?>
					<div class="product__head-advantage product__head-advantage_waranty product__head-advantage_ten-years">
						<div class="product__head-advantage-left">ГАРАНТИЙНЫЙ <br> СРОК СЛУЖБЫ</div>
						<div class="product__head-advantage-right"><span>10</span> ЛЕТ</div>
					</div>
				<?php endif; ?>

				<?php if ($adv_fifteen): ?>
					<div class="product__head-advantage product__head-advantage_waranty product__head-advantage_fifteen-years">
						<div class="product__head-advantage-left">ГАРАНТИЙНЫЙ <br> СРОК СЛУЖБЫ</div>
						<div class="product__head-advantage-right"><span>15</span> ЛЕТ</div>
					</div>
				<?php endif; ?>

				<?php if ($adv_streight): ?>
					<div class="product__head-advantage product__head-advantage_streight">ВЫСОКАЯ ИЗНОСОСТОЙКОСТЬ</div>
				<?php endif; ?>

				<?php if ($adv_simple): ?>
					<div class="product__head-advantage product__head-advantage_simple">ПРОСТОТА МОНТАЖНЫХ РАБОТ</div>
				<?php endif; ?>

				<?php if ($adv_color): ?>
					<div class="product__head-advantage product__head-advantage_color">ЦВЕТОВОЕ РАЗНООБРАЗИЕ</div>
				<?php endif; ?>

				<?php if ($adv_cor_streight): ?>
					<div class="product__head-advantage product__head-advantage_corr-streight">ВЫСОКАЯ КОРРОЗИОННАЯ СТОЙКОСТЬ</div>
				<?php endif; ?>

				<?php if ($adv_wallet): ?>
					<div class="product__head-advantage product__head-advantage_wallet1">ДОСТУПНАЯ ЦЕНА</div>
				<?php endif; ?>

				<?php if ($adv_fire): ?>
					<div class="product__head-advantage product__head-advantage_fire">ВЫСОКАЯ ПОЖАРОСТОЙКОСТЬ</div>
				<?php endif; ?>
			</div>
		<?php
	}
?>