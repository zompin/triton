<?php
	if (is_category()) {
		$breadcrumbs_title = single_cat_title('', 0);
	} else {
		$breadcrumbs_title = get_the_title();
	}

	global $division_name;
	global $division_url;
	global $self;
?>

<div class="breadcrumbs">
	<div class="breadcrumbs__title">
		<?php echo $division_name; ?>
	</div>
	<div class="breadcrumbs__path">
		<a href="<?php echo home_url('/'); ?>" class="breadcrumbs__item">ГЛАВНАЯ</a>
		<span class="breadcrumbs__div"></span>
		<?php
			if ($division_url && $division_name) {
				?>
					<a href="<?php echo home_url($division_url); ?>" class="breadcrumbs__item">
						<?php echo $division_name ?>
					</a>
					<span class="breadcrumbs__div"></span>
				<?php
			}

			if (is_single() && in_parent_category('produktsiya', get_the_ID())) {
				$parent_category = get_the_category();
				if ($parent_category) {
					$parent_category = $parent_category[0];
					
					if ($parent_category->slug != 'produktsiya') {
						?>
							<a href="<?php echo home_url('/category/' . $parent_category->slug); ?>" class="breadcrumbs__item">
								<?php echo $parent_category->name; ?>
							</a>
							<span class="breadcrumbs__div"></span>
						<?php
					}
				}
			}
		?>
		<span class="breadcrumbs__current">
			<?php 
				if ($self) {
					echo $division_name;
				} else {
					echo $breadcrumbs_title;
				}
			 ?>
		</span>
	</div>
</div>