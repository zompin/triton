<?php
	$thumb = get_post_meta(get_the_ID(), 'thumb', true);
?>
<div class="preview preview_information">
	<a href="<?php the_permalink(); ?>" class="preview__inner preview__inner_information" style="background-image: url(<?php echo $thumb; ?>);"></a>
	<div class="preview__desc preview__desc_information">
		<h2 class="preview__title preview__title_information">
			<?php the_title(); ?>
		</h2>
		<p class="preview__about preview__about_information">
			<?php echo strip_tags(get_the_content()); ?>
		</p>
		<a href="<?php the_permalink(); ?>" class="preview__more preview__more_information">Подробнее</a>
	</div>
</div>