var gulp = require('gulp');
var less = require('gulp-less');
var bs = require('browser-sync').create();
var sourcemaps = require('gulp-sourcemaps')

gulp.task('default', ['bs', 'less'], function() {
	gulp.watch('*.less', ['less']);
});

gulp.task('prod', function() {
	return gulp.src('./*.less')
		.pipe(less({compress: true}))
		.pipe(gulp.dest('./'));
});

gulp.task('less', function() {
	return gulp.src('./*.less')
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./'));
});

gulp.task('bs', function() {
	bs.init({
		notify: false,
		proxy: 'triton',
		files: ['*.css', '*.php', 'inc/*.php', 'js/*.js']
	});
});