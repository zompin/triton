;(function() {
	var ymapi 	= document.createElement('script');

	ymapi.src 	= 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
	ymapi.onload 	= function() {
		var map = document.getElementById('map');

		if (map) {
			var myMap;
			ymaps.ready(function() {
				myMap = new ymaps.Map(map, {
					center: [49.816382, 73.095289],
					zoom: 18,
					//controls: []
				});
				var template = ymaps.templateLayoutFactory.createClass('<div id="map__mark"></div>');
				var placeMark = new ymaps.Placemark(
						myMap.getCenter(), {}, {
							iconLayout: template
						}
					);
				myMap.geoObjects.add(placeMark);
				myMap.behaviors.disable("scrollZoom");
			});
		}
	}

	setTimeout(function() {
		document.head.appendChild(ymapi);
	}, 100);
})();