<?php
	$id 			= $post->ID;
	$custom 		= get_post_meta($id, 'custom', true) * 1;
	$custom_name 	= get_post_meta($id, 'custom_name', true);
	$custom_text 	= get_post_meta($id, 'custom_text', true);
?>
<div class="metabox__tab">
	<input type="hidden" name="extra[custom]" value="">
	<input id="custom" type="checkbox" name="extra[custom]" value="1" <?php if ($custom) echo 'checked' ?>>
	<label for="custom">Вкладка общего назначения</label>
	<div class="metabox__tab-inner custom">
		<label class="metabox__label">
			Название закладки
			<input type="text" name="extra[custom_name]" value="<?php echo $custom_name; ?>">
		</label>
		<label class="metabox__label">
			Область применения
			<textarea name="extra[custom_text]"><?php echo $custom_text; ?></textarea>
		</label>
	</div>
</div>