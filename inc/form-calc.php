<div class="form__fixed form__fixed_calc">
	<div class="form__close-div"></div>
	<div class="form__cont">
		<div class="form form_calc">
			<button class="form__close">&times;</button>
			<div class="form__header">ЗАЯВКА НА ДЕТАЛЬНЫЙ РАСЧЁТ</div>
			<label class="form__label form__label_wide">
				<span class="form__label-text form__label-text_partner form__label-text_require">Имя</span>
				<input type="text" class="form__input form__input_calc form__input_calc-name form_require">
			</label>
			<label class="form__label form__label_wide">
				<span class="form__label-text form__label-text_partner form__label-text_require">Ваш е-mail</span>
				<input type="text" class="form__input form__input_calc form__input_calc-email form_require">
			</label>
			<label class="form__label form__label_wide">
				<span class="form__label-text form__label-text_partner form__label-text_require">Телефон</span>
				<input type="text" class="form__input form__input_calc form__input_calc-phone form_require" placeholder="+7(___)___-__-__">
			</label>
			<label class="form__label form__label_wide">
				<span class="form__label-text">Необходимый материал</span>
				<textarea class="form__textarea form__textarea_partner form__textarea_calc_material"></textarea>
			</label>
			<div class="form__label form__label_wide">
				<span class="form__label-text">Вложить файл</span>
				<div class="form__file-control">
					<div class="form__file-cont" data-text="Выбрать">
						<input type="file" class="form__file form__file_calc form__file_calc_file" multiple>
						<div class="form__file-files"></div>
					</div>
					<button class="form__button form__button-remove"></button>
				</div>
			</div>
			<label class="form__label form__label_wide">
				<span class="form__label-text form__label-text_calc-captcha form__label-text_require">Код на картинке</span>
				<div class="form__captcha-cont">
					<div class="form__captcha form__captcha_calc">
						<img src="<?php echo get_template_directory_uri() . '/captcha/captcha.php' ?>" class="form__captcha-img">
					</div>
					<input type="text" class="form__input form__input_captcha form__input_captcha-calc form_require">
					<div class="form__text form__text_captcha-calc">Введите символы, которые показаны на картинке</div>
				</div>
			</label>
			<div>
				<button class="form__button form__button_calc">ОТПРАВИТЬ ЗАЯВКУ</button>
				<span class="form__label-text form__label-text_require"></span>
				<span class="form__text"> - поля обязательные для заполнения</span>
			</div>
		</div>
	</div>
</div>