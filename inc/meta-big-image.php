<?php
	$id = $post->ID;
	$image = get_post_meta($id, 'image', true);
?>

<div class="metabox__tab">
	<label>
		Основное (большое) изображение
		<textarea name="extra[image]"><?php echo $image; ?></textarea>
	</label>
</div>