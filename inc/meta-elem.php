<?php
	$id = $post->ID;

	$elem = get_post_meta($id, 'elem', true);
	$elem_image = get_post_meta($id, 'elem_image', true);
	$elem_text = get_post_meta($id, 'elem_text', true);
?>

<div class="metabox__tab elem">
	<input type="hidden" name="extra[elem]" value="">
	<input type="checkbox" id="elem-tab" name="extra[elem]" value="1" <?php if ($elem) echo 'checked'; ?>>
	<label for="elem-tab">Доборные элементы</label>
	<div class="metabox__tab-inner">
		<label class="metabox__label">
			Текст в заголовке
			<textarea name="extra[elem_text]"><?php echo $elem_text; ?></textarea>
		</label>
		<label class="metabox__label">
			Изображение доборных элементов
			<textarea name="extra[elem_image]"><?php echo $elem_image; ?></textarea>
		</label>
	</div>
</div>