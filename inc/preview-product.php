<div class="preview">
	<div class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_post_meta($id, 'thumb', true); ?>);">
		<a href="<?php the_permalink(); ?>" class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</a>
		<div class="preview__title-img">
			<?php the_title(); ?>
		</div>
	</div>
</div>