<?php get_header(); ?>

<?php
	global $self;
	global $division_name;
	global $img;
	global $wp_query;
	
	$self = true;
	$division_name = 'Поиск';
	$img = get_template_directory_uri() . '/imgs/search-image.jpg';
	$count = $wp_query->found_posts;

?>

<?php get_template_part('inc/breadcrumbs'); ?>
<?php get_template_part('inc/image'); ?>

<main class="wrapper">
	<div class="search">
		<h1 class="search__header">
			Вы искали:
			<span class="search__header-value">
				<?php echo get_search_query(); ?>
			</span>
		</h1>

		<form class="search__form">
			<div class="search__form-header">Новый поиск</div>
			<div class="search__form-row">
				<input name="s" type="text" class="search__form-input" value="<?php echo get_search_query(); ?>" placeholder="Введите запрос">
				<button type="submit" class="search__form-button">
					<span>Найти</span>
				</button>
			</div>
		</form>

		<div class="search__result">
			<div class="search__result-count">
				<?php if ($count): ?>
					Найдено:
					<span>
						<?php echo $count; ?>
					</span>
					<?php echo sklon($count, 'результат', 'релузьтата', 'результатов'); ?>
				<?php else: ?>
					Ничего не найдено
				<?php endif; ?>
			</div>
		</div>
		<div class="search__items">
			<?php if (have_posts()): ?>

				<?php
					while (have_posts()) {
						the_post();
						$id = get_the_ID();
						$title = get_the_title($id);
						$content = strip_tags(get_the_content($id));
						$link = get_the_permalink();

						?>
							<div class="search__item">
								<div class="search__item-header">
									<?php echo $title; ?>
								</div>
								<div class="search__item-content">
									<?php echo $content; ?>
								</div>
								<a href="<?php echo $link; ?>" class="search__item-link">
									<?php echo urldecode($link); ?>
								</a>
							</div>
						<?php
					}
				?>

			<?php else: ?>
				<div class="search__empty">Ничего не найдено</div>
			<?php endif; ?>
		</div>
	</div>
	
</main>

<?php get_footer(); ?>