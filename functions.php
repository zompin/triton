<?php

function twentyfifteen_setup() {

	add_theme_support( 'title-tag' );

	register_nav_menus( array(
		'primary' => 'Главное меню',
		'company' => 'Меню о компании',
		'information' => 'Меню информации',
		'product' => 'Меню продукции'
	) );

}
add_action( 'after_setup_theme', 'twentyfifteen_setup' );

function triton_scripts() {
	$version = '17102017';

	wp_enqueue_style( 'main-style', get_stylesheet_uri(), null, $version );

	wp_enqueue_script( 'jq', get_template_directory_uri() . '/js/jquery/dist/jquery.min.js', null, null, true );
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick-carousel/slick/slick.min.js', array('jq'), null, true );
	wp_enqueue_script( 'dot', get_template_directory_uri() . '/js/dotdotdot.min.js', array('jq'), null, true );
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jq', 'slick', 'dot'), $version, true);
	wp_enqueue_script( 'map', get_template_directory_uri() . '/js/map.js', null, null, true);
}
add_action( 'wp_enqueue_scripts', 'triton_scripts' );

function remote_file_size($url) {
	if (strpos($url, 'http') === false) return 0;

	$head = array_change_key_case(get_headers($url, true));
	$filesize = $head['content-length'];

	if ($filesize >= 1000000) {
		$filesize = round($filesize / 1000000, 2) . ' MB';

	} elseif ($filesize >= 1000) {
		$filesize = round($filesize / 1000, 2) . ' KB';
	}

	return $filesize;
}

function show_file($title, $urls, $names) {
	

	if ($urls && $names): ?>
		<h2><?php echo $title; ?></h2>
		<div class="information">
			<?php foreach ($urls as $k => $v): ?>
				<?php
					$url = $urls[$k];
					$name = $names[$k];
				?>
				<a href="<?php echo $url ?>" class="information__item">
					<div class="information__title"><?php echo $name; ?></div>
					<div class="information__size">
						<?php
							echo remote_file_size($url);
						?>
					</div>
				</a>
			<?php endforeach; ?>
		</div>
	<?php endif;
}

function in_parent_category($cat, $post){
	$post_category = get_the_category($post);
	$post_category = $post_category ? $post_category[0] : 0;

	do {
		$post_category = get_category($post_category->parent);
	} while (!$post_category->errors && $post_category->parent && $post_category->parent !== 0);

	return !$post_category->errors && $post_category->slug === $cat;
}

function product_form() {
	$screens = array('post', 'page');
	
	foreach ($screens as $screen) {
		add_meta_box('product_box', 'Блок встроенной верстки', 'product_meta_box', $screen);
	}
}
add_action('add_meta_boxes', 'product_form');

function product_meta_box($post) {
	?>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/metabox.css'; ?>">
		<script src="<?php echo get_template_directory_uri() . '/js/jquery-ui.min.js' ?>"></script>
		<script src="<?php echo get_template_directory_uri() . '/js/admin.js' ?>"></script>
		<div class="metabox">
			<?php include 'inc/meta-thumb.php' ?>
			<?php include 'inc/meta-product-image.php'; ?>
			<?php include 'inc/meta-part-desc.php' ?>
			<?php include 'inc/meta-icons.php'; ?>
			<?php include 'inc/meta-advantages.php'; ?>
			<?php include 'inc/meta-table.php'; ?>
			<?php include 'inc/meta-color.php'; ?>
			<?php include 'inc/meta-tech.php'; ?>
			<?php include 'inc/meta-elem.php'; ?>
			<?php include 'inc/meta-oblits.php'; ?>
			<?php include 'inc/meta-install.php'; ?>
			<?php include 'inc/meta-vodostok.php'; ?>
			<?php include 'inc/meta-oblast.php'; ?>
			<?php include 'inc/meta-before-tabs.php'; ?>
			<?php include 'inc/meta-komplekt.php'; ?>
			<?php include 'inc/meta-project.php'; ?>
			<?php include 'inc/meta-big-image.php'; ?>
			<?php include 'inc/meta-cert.php'; ?>
			<?php include 'inc/meta-docs.php'; ?>
		</div>
		<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
	<?php
}

function my_extra_fields_update( $post_id ){
	if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
	if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

	if( !isset($_POST['extra']) ) return false; 

	// Все ОК! Теперь, нужно сохранить/удалить данные

	//$_POST['extra'] = array_map('trim', $_POST['extra']);
	foreach( $_POST['extra'] as $key=>$value ){

		if (is_array($value)) {
			$value = array_map('trim', $value);
			$value = array_map('removeHost', $value);
		} else {
			$value = trim($value);
			$value = removeHost($value);
		}

		if( empty($value) ){
			delete_post_meta($post_id, $key); // удаляем поле если значение пустое
			continue;
		}

		update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
	}

	return $post_id;
}
add_action('save_post', 'my_extra_fields_update', 0);

function sklon($count, $form1, $form2, $form3) {
	if ($count % 10 == 1) {
		return $form1;
	} elseif ($count % 10 > 1 && $count % 10 < 5) {
		return $form2;
	} else {
		return $form3;
	}
}

function exclude($query) {
	if ($query->is_search && !is_admin()) {

		$query->set('category__not_in', array(5));
	}
}
add_action('pre_get_posts', 'exclude');

function removeHost($url) {
	$pos = strpos($url, '//');

	if ($pos !== false && strlen($url) >= $pos) {
		$pos = strpos($url, '/', $pos + 2);
		
		if ($pos !== false) {
			$url = substr($url, $pos);
		}
	}

	return $url;
}