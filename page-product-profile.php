<?php /*Template name: Продукция, Профиль строительный*/ ?>
<?php get_header(); ?>

<?php
	$division_name = 'Продукция';
	$division_url = '/category/produktsiya/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<main class="wrapper">
	<aside class="aside aside_product">
		<?php get_template_part('inc/product-menu'); ?>
	</aside>
	<div class="content content_product">
		<?php get_template_part('inc/image'); ?>
		<?php
			the_post();
			the_content();
		?>
		<br>
		<?php get_template_part('inc/single-form'); ?>
	</div>
</main>

<?php get_footer(); ?>