<?php get_header(); ?>

<?php
	global $division_name;
	global $division_url;
	
	$division_name = 'Информация';
	$division_url = '/category/poleznaya-informatsiya/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<?php get_template_part('inc/image'); ?>

<main class="wrapper">

	<aside class="aside">
		<?php
			wp_nav_menu(
				array(
					'theme_location' => 'information',
					'fallback_cb' => '__return_empty_string',
					'depth' => 1,
					'container' => '',
					'menu_id' => '',
					'menu_class' => 'aside__menu'
				)
			);
		?>
	</aside>
	<div class="content">
		<?php
			the_post();
			the_content();
		?>
		<a href="<?php echo home_url('/category/poleznaya-informatsiya/'); ?>" class="information__link">
			Все статьи
		</a>
	</div>
</main>

<?php get_footer(); ?>