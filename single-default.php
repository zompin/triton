<?php get_header(); ?>

<?php
	$thumb = get_post_meta(get_the_ID(), 'thumb', true);
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<?php get_template_part('inc/image'); ?>

<main class="wrapper">
	<div class="content content_default">

		<?php if ($thumb): ?>
			<div class="news__head">
				<div class="news__thumb">
					<img src="<?php echo $thumb; ?>">
				</div>
			</div>
		<?php endif; ?>
		<?php
			the_post();
			the_content();
		?>
	</div>
</main>

<?php get_footer(); ?>