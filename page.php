<?php /*Template name: Вопрос-Ответ*/ ?>
<?php get_header(); ?>
<?php $div_title = 'Инофрмация'; ?>

<?php get_template_part('inc/breadcrumbs'); ?>
<?php get_template_part('inc/image'); ?>

<main class="wrapper">
	<div class="content content_default">
		<?php
			the_post();
			the_content();
		?>
	</div>
</main>

<?php get_footer(); ?>