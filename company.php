<?php /*Template name: Компания*/ ?>
<?php get_header(); ?>

<?php
	$division_name = 'Компания';
	$division_url = '/o-kompanii/';
?>

<?php get_template_part('inc/breadcrumbs'); ?>
<?php get_template_part('inc/image'); ?>

<main class="wrapper">

	<aside class="aside">
	<?php
		wp_nav_menu(
			array(
				'theme_location' => 'company',
				'fallback_cb' => '__return_empty_string',
				'depth' => 1,
				'container' => '',
				'menu_id' => '',
				'menu_class' => 'aside__menu'
			)
		);
	?>
	</aside>
	<div class="content content_company">
		<?php
			the_post();
			the_content();
			$id 			= get_the_ID();
			$certs_images 	= get_post_meta($id, 'cert_img', true);
			$certs_names 	= get_post_meta($id, 'cert_name', true);
			$certs 			= get_post_meta($id, 'certs', true) * 1;

			if (count($certs_images) == count($certs_names) && $certs) {
				?>
					<div class="cert">
						<?php 
							foreach ($certs_images as $k => $v) {
								?>
									<div class="cert__item">
										<div class="cert__item-image">
											<img src="<?php echo $certs_images[$k]; ?>">
										</div>
										<div class="cert__item-title">
											<?php echo $certs_names[$k]; ?>
										</div>
									</div>
								<?php
							}
						?>
					</div>
				<?php
			}
		?>
	</div>
</main>

<?php get_footer(); ?>