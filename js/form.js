;(function($) {
	var formUrl = '/wp-content/themes/bmk/send.php';

	$('.form__add-cv').click(function() {
		var exp = $('.form__expirience').eq(0).clone();
		exp.find('input').val('');

		$(this).parent().before(exp);
	});

	$(document).on('click', '.form__expririence-remove', function(e) {
		$(e.target).parent().remove();
	});

	$('.form__button_dillers').click(function() {
		var data = {};
		var checked = true;
		var name = $('.form__input_dillers_name').val();
		var company = $('.form__input_dillers_company').val();
		var phone = $('.form__input_dillers_phone').val();
		var email = $('.form__input_dillers_email').val();
		var city = $('.form__input_dillers_city').val();
		var message = $('.form__input_dillers_message').val();
		var code = $('.form__input_dillers_code').val();

		data.name = name;
		data.company = company;
		data.phone = phone;
		data.email = email;
		data.city = city;
		data.message = message;
		data.code = code;
		data.type = 'dillers';

		$('.form_dillers .form_require').each(function(i, e) {
			if ($(e).val().length == 0) {
				checked = false
				$(e).addClass('form_error');
			} else {
				$(e).removeClass('form_error');
			}
		});

		ajax(data, false, this, checked, 'Ваша заявка отправлена');
	});

	$('.button_send-cv').click(function() {
		var fd = new FormData();
		var checked = true;

		var lname = $('.form__input_cv-lname').val();
		var fname = $('.form__input_cv-fname').val();
		var bday = $('.form__input_cv-day').val();
		var bmonth = $('.form__input_cv-month').val();
		var byear = $('.form__input_cv-year').val();
		var area = $('.form__input_cv-area').val();
		var city = $('.form__input_cv-city').val();
		var street = $('.form__input_cv-street').val();
		var home = $('.form__input_cv-home').val();
		var q = $('.form__input_cv-q').val();
		var phone = $('.form__input_cv-phone').val();
		var email = $('.form__input_cv-email').val();
		var status = $('.form__input_cv-status:checked').val();
		var kids = $('.form__input_cv-kids:checked').val();
		var edType = $('.form__input_cv-educ-type:checked').val();
		var univ = $('.form__input_cv-univ').val();
		var spec = $('.form__input_cv-spec').val();
		var comp = $('.form__input_comp:checked').val();
		var kz = $('.form__input-lang-kz').prop('checked') * 1;
		var ru = $('.form__input-lang-ru').prop('checked') * 1;
		var en = $('.form__input-lang-en').prop('checked') * 1;
		var other = $('.form__input-lang-other').prop('checked') * 1;
		var otherlang = $('.form__input_lang-other-name').val();
		var drive = $('.form__input_drive:checked').val();
		var priv = $('.form__input_cv-priv').val();
		var hob = $('.form__input_cv-hob').val();
		var specArea = $('.form__input_cv-cpec-area').val();
		var fplace = $('.form__input_cv-fplace').val();
		var pay = $('.form__input_cv-pay').val();
		var code = $('.form__input_captcha_cv').val();
		var file = $('.form__file-photo-file')[0].files;

		fd.append('lname', lname);
		fd.append('fname', fname);
		fd.append('bday', bday);
		fd.append('bmonth', bmonth);
		fd.append('byear', byear);
		fd.append('area', area);
		fd.append('city', city);
		fd.append('street', street);
		fd.append('home', home);
		fd.append('q', q);
		fd.append('phone', phone);
		fd.append('email', email);
		fd.append('status', status);
		fd.append('kids', kids);
		fd.append('edType', edType);
		fd.append('univ', univ);
		fd.append('spec', spec);
		fd.append('comp', comp);
		fd.append('kz', kz);
		fd.append('ru', ru);
		fd.append('en', en);
		fd.append('type', 'cv');

		if (other) fd.append('otherlang', otherlang);

		fd.append('drive', drive);
		fd.append('priv', priv);
		fd.append('hob', hob);
		fd.append('specArea', specArea);
		fd.append('fplace', fplace);
		fd.append('pay', pay);
		fd.append('code', code);

		$('.form__expirience').each(function(i, e) {
			var period = $(e).find('.form__input_cv-period').val();
			var company = $(e).find('.form__input_cv-company').val();
			var location = $(e).find('.form__input_cv-location').val();
			var area = $(e).find('.form__input_cv-company-area').val();
			var place = $(e).find('.form__input_cv-place').val();
			var data = {};

			data.period = period;
			data.company = company;
			data.location = location;
			data.area = area;
			data.place = place;

			fd.append('exp[]', JSON.stringify(data));
		});


		if (file) {
			fd.append('photo', file[0]);
		}

		$('.form_cv .form_require').each(function(i, e) {
			if ($(e).val().length == 0) {
				checked = false
				$(e).addClass('form_error');
			} else {
				$(e).removeClass('form_error');
			}
		});

		ajax(fd, true, this, checked, 'Ваше резюме отправлено');
	});

	function ajax(data, isFd, button, checked, message, callback) {
		var sendObj = {
			type: 'POST',
			url: formUrl,
			data: data,
			cache: false,
			success: function(res) {
				$(button).prop('disabled', false);

				if (res == '1') {
					alert(message);
					if (callback) {
						callback();
					}
				} else if (res == '0') {
					alert('Не удалось отправить запрос');
				} else {
					alert(res);
				}
			},
			error: function() {
				$(button).prop('disabled', false);
				alert('Произошла ошибка.');
			}
		}

		if (isFd) {
			sendObj.contentType = false;
			sendObj.processData = false;
		}

		if (!checked) {
			alert('Вы не заполнини обязательные поля');
		} else {

			$(button).prop('disabled', true);

			$.ajax(sendObj);
		}
	}
})(jQuery);