<?php
	$id = $post->ID;
	$before_tabs = get_post_meta($id, 'before_tabs', true);
?>

<div class="metabox__tab">
	<label>
		Блок перед вкладками
		<textarea name="extra[before_tabs]"><?php echo $before_tabs; ?></textarea>
	</label>
</div>