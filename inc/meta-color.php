<?php
	$id = $post->ID;
	$colors = get_post_meta($id, 'colors', true) * 1;
	$color_thumb = get_post_meta($id, 'color_thumb', true);
	$color_name = get_post_meta($id, 'color_name', true);
	$color_code = get_post_meta($id, 'color_code', true);
	$color_type = get_post_meta($id, 'color_type', true);
	$color_text = get_post_meta($id, 'color_text', true);
	$color_item_type = get_post_meta($id, 'color_item_type', true);
?>
<div class="metabox__tab color">
	<input type="hidden" name="extra[colors]" value="">
	<input type="checkbox" id="color-tab" name="extra[colors]" value="1" <?php if ($colors) echo 'checked'; ?>>
	<label for="color-tab">Цвет</label>
	<div class="metabox__tab-inner">
		<label class="metabox__label">
			Текст в заголовки вкладки
			<textarea name="extra[color_text]"><?php echo $color_text; ?></textarea>
		</label>
		<?php
			if ($color_thumb && $color_name && $color_code) {
				foreach ($color_thumb as $k => $v) {
					?>
						<?php if ($color_item_type[$k]): ?>
							<div class="color__item-color">
								<input type="hidden" name="extra[color_item_type][]" value="1">
								<input type="hidden" name="extra[color_thumb][]" value="">
								<input type="hidden" name="extra[color_code][]" value="">
								<input type="hidden" name="extra[color_type][]" value="">
								<label class="metabox__label">
									Заголовок
									<textarea name="extra[color_name][]"><?php echo $color_name[$k]; ?></textarea>
								</label>
								<button type="button" class="button button_remove-color">Удалить заголовок</button>
							</div>
						<?php else: ?>
							<div class="color__item">
								<input type="hidden" name="extra[color_item_type][]" value="">
								<label class="metabox__label">
									Изображение цвета
									<textarea name="extra[color_thumb][]"><?php echo $color_thumb[$k]; ?></textarea>
								</label>
								<label class="metabox__label">
									Название цвета
									<textarea name="extra[color_name][]"><?php echo $color_name[$k]; ?></textarea>
								</label>
								<label class="metabox__label">
									Код цвета
									<textarea name="extra[color_code][]"><?php echo $color_code[$k]; ?></textarea>
								</label>
								<select name="extra[color_type][]" value="<?php echo $color_type[$k]; ?>">
									<option value="0">Светлая надпись</option>
									<option value="1" <?php if ($color_type[$k]) echo 'selected'; ?>>Темная надпись</option>
								</select>
								<button type="button" class="button button_remove-color">Удалить цвет</button>
							</div>
						<?php endif; ?>
					<?php
				}
			} else {
				?>
					<div class="color__item">
						<input type="hidden" name="extra[color_item_type][]" value="">
						<label class="metabox__label">
							Изображение цвета
							<textarea name="extra[color_thumb][]"></textarea>
						</label>
						<label class="metabox__label">
							Название цвета
							<textarea name="extra[color_name][]"></textarea>
						</label>
						<label class="metabox__label">
							Код цвета
							<textarea name="extra[color_code][]"></textarea>
						</label>
						<select name="extra[color_type][]">
							<option value="0">Светлая надпись</option>
							<option value="1">Темная надпись</option>
						</select>
						<button type="button" class="button button_remove-color">Удалить цвет</button>
					</div>
				<?php
			}
		?>
		<div class="color__button-cont">
			<button type="button" class="button button_add-color">Добавить цвет</button>
			<button type="button" class="button button_add-header-color">Добавить заголовок</button>
		</div>
	</div>
</div>