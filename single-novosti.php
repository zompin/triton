<?php get_header(); ?>

<?php
	global $division_name;
	global $division_url;
	
	$division_name = 'Новости';
	$division_url = '/category/novosti/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<?php get_template_part('inc/image'); ?>

<div class="news">
	<a href="<?php echo home_url('/category/novosti'); ?>" class="news__link">К СПИСКУ НОВОСТЕЙ</a>
	<div class="news__head">
		<div class="news__thumb">
			<?php
				$post_thumb = get_post_meta(get_the_ID(), 'thumb', true);
			?>
			<img src="<?php echo $post_thumb; ?>">
		</div>
		<div class="news__title">
			<div class="news__title-inner">
				<?php the_title(); ?>
			</div>
		</div>
	</div>
	<div class="news__content">
		<?php the_post(); ?>
		<?php the_content(); ?>
	</div>
	<div class="news__nav">
		<?php
			$next_post = get_next_post(true);
			$prev_post = get_previous_post(true);
		?>

		<?php if ($next_post): ?>
			<a href="<?php echo get_permalink($next_post->ID); ?>" class="news__nav-item news__nav-prev">
				<div class="news__nav-thumb news__nav-thumb-prev">
					<img src="<?php echo get_post_meta($next_post->ID, 'thumb', true); ?>">
				</div>
				<div class="news__nav-cont">
					<div class="news__nav-caption">Следующая новость</div>
					<div class="news__nav-title">
						<?php echo $next_post->post_title; ?>
					</div>
				</div>
			</a>
		<?php else: ?>
			<div class="news__nav-item"></div>
		<?php endif; ?>
		<?php if ($prev_post): ?>
			<a href="<?php echo get_permalink($prev_post->ID); ?>" class="news__nav-item news__nav-next">
				<div class="news__nav-cont">
					<div class="news__nav-caption">Предыдущая новость</div>
					<div class="news__nav-title">
						<?php echo $prev_post->post_title; ?>
					</div>
				</div>
				<div class="news__nav-thumb news__nav-thumb-next">
					<img src="<?php echo get_post_meta($prev_post->ID, 'thumb', true); ?>">
				</div>
			</a>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>