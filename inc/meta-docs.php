<?php
	$id 				= $post->ID;
	$docs 				= get_post_meta($id, 'docs', true) * 1;
	$docs_install_url 	= get_post_meta($id, 'docs_install_url', true);
	$docs_install_name 	= get_post_meta($id, 'docs_install_name', true);
	$docs_bro_url 		= get_post_meta($id, 'docs_bro_url', true);
	$docs_bro_name 		= get_post_meta($id, 'docs_bro_name', true);
	$docs_install 		= get_post_meta($id, 'docs_install', true);
	$docs_bro 			= get_post_meta($id, 'docs_bro', true);
?>
<div class="metabox__tab">
	<input type="hidden" name="extra[docs]" value="">
	<input id="docs" type="checkbox" name="extra[docs]" value="1" <?php if ($docs) echo 'checked' ?>>
	<label for="docs">Документы</label>
	<div class="metabox__tab-inner docs">
		<div>
			<input type="hidden" name="extra[docs_install]" value="">
			<input type="checkbox" name="extra[docs_install]" id="docs-install" value="1" <?php if ($docs_install) echo 'checked' ?>>
			<label for="docs-install">Инструкции</label>
			<div class="install-div">
				<?php
					if ($docs_install_url && $docs_install_name) {
						?>
							<div class="docs_install">
								<?php
									foreach ($docs_install_url as $k => $v) {
										?>
											<div class="docs__item-install">
												<label class="metabox__label">
													Название инструкции
													<textarea name="extra[docs_install_name][]"><?php echo $docs_install_name[$k]; ?></textarea>
												</label>
												<label class="metabox__label">
													Ссылка на инструкцию
													<textarea name="extra[docs_install_url][]"><?php echo $docs_install_url[$k]; ?></textarea>
												</label>
												<button type="button" class="button button_docs-install-remove">Удалить инструкцию</button>
											</div>
											<div class="button-cont">
												<button type="button" class="button button_docs-install-add">Добавить инструкцию</button>
											</div>
										<?php
									}
								?>
							</div>
						<?php
					} else {
						?>
							<div class="docs_install">
								<div class="docs__item-bro">
									<label class="metabox__label">
										Название инструкции
										<textarea name="extra[docs_install_name][]"></textarea>
									</label>
									<label class="metabox__label">
										Ссылка на инструкцию
										<textarea name="extra[docs_install_url][]"></textarea>
									</label>
									<button type="button" class="button button_docs-install-remove">Удалить инструкцию</button>
								</div>
								<div class="button-cont">
									<button type="button" class="button button_docs-install-add">Добавить инструкцию</button>
								</div>
							</div>
						<?php
					}
				?>
			</div>
		</div>
		<div>
			<input type="hidden" name="extra[docs_bro]" value="">
			<input type="checkbox" name="extra[docs_bro]" id="docs-bro" value="1" <?php if ($docs_bro) echo 'checked' ?>>
			<label for="docs-bro">Брошюры</label>
			<div class="install-div">
				<?php
					if ($docs_bro_url && $docs_bro_name) {
						?>
							<div class="docs_bro">
								<?php
									foreach ($docs_bro_url as $k => $v) {
										?>
											<div class="docs__item-bro">
												<label class="metabox__label">
													Название брошюры
													<textarea name="extra[docs_bro_name][]"><?php echo $docs_bro_name[$k]; ?></textarea>
												</label>
												<label class="metabox__label">
													Ссылка на инструкцию
													<textarea name="extra[docs_bro_url][]"><?php echo $docs_bro_url[$k]; ?></textarea>
												</label>
												<button type="button" class="button button_docs-bro-remove">Удалить брошюру</button>
											</div>
											<div class="button-cont">
												<button type="button" class="button button_docs-bro-add">Добавить брошюру</button>
											</div>
										<?php
									}
								?>
							</div>
						<?php
					} else {
						?>
							<div class="docs_bro">
								<div class="docs__item-bro">
									<label class="metabox__label">
										Название брошюры
										<textarea name="extra[docs_bro_name][]"></textarea>
									</label>
									<label class="metabox__label">
										Ссылка на брошюру
										<textarea name="extra[docs_bro_url][]"></textarea>
									</label>
									<button type="button" class="button button_docs-bro-remove">Удалить брошюру</button>
								</div>
								<div class="button-cont">
									<button type="button" class="button button_docs-bro-add">Добавить брошюру</button>
								</div>
							</div>
						<?php
					}
				?>
			</div>
		</div>
	</div>
</div>