<?php get_header(); ?>

<main>
	<?php
		$division_name = 'Новости ';
	?>
	<?php get_template_part('inc/breadcrumbs'); ?>

	<div class="previews previews-news-category">
		<?php
			if(have_posts()) {
				while (have_posts()) {
					the_post();
					get_template_part('inc/preview-news');
				}
			}
		?>
	</div>
</main>

<?php get_footer(); ?>