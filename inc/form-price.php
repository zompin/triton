<div class="form__fixed form__fixed_price">
	<div class="form__close-div"></div>
	<div class="form__cont">
		<div class="form form_price">
			<button class="form__close">&times;</button>
			<div class="form__header">СКАЧАТЬ ПРАЙС</div>
			<label class="form__label">
				<span class="form__label-text form__label-text_price form__label-text_require">Имя</span>
				<input type="text" class="form__input form_require form__input_price form__input_price-name">
			</label>
			<label class="form__label">
				<span class="form__label-text form__label-text_price form__label-text_require">Телефон</span>
				<input type="text" class="form__input form_require form__input_price form__input_price-phone">
			</label>
			<label class="form__label">
				<span class="form__label-text form__label-text_price form__label-text_require">Ваш e-mail</span>
				<input type="text" class="form__input form_require form__input_price form__input_price-email">
			</label>
			<div class="form__captcha">
				<img src="<?php echo get_template_directory_uri() . '/captcha/captcha.php' ?>" class="form__captcha-img">
			</div>
			<label class="form__label form__label_after-captcha">
				<span class="form__label-text form__label-text_price form__label-text_require">Код на картинке</span>
				<input type="text" class="form__input form_require form__input_price form__input_price-captcha">
			</label>
			<div class="form__text">Введите символы, которые показаны на картинке</div>
			<button class="form__button form__button_price">ОТПРАВИТЬ</button>
			<div class="form__text">После нажатия кнопки "Отправить" скачивание прайса начнется автоматически</div>
		</div>
	</div>
</div>