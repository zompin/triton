<?php /*Template name: Дилерам*/ ?>
<?php get_header(); ?>

<?php
	$division_name = 'Дилерам';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<?php get_template_part('inc/image'); ?>

<main class="wrapper">
	<div class="content content_dillers">
		<?php
			the_post();
			the_content();
		?>
		<h2>СТАТЬ ДИЛЕРОМ</h2>
		<div class="form form_dillers">
			<label class="form__label form__label_dillers">
				<span class="form__label-text form__label-text_require">Ф.И.О:</span>
				<input type="text" class="form__input form__input_dillers form__input_dillers_name form_require">
			</label>
			<label class="form__label form__label_dillers">
				<span class="form__label-text form__label-text_require">Название компании:</span>
				<input type="text" class="form__input form__input_dillers form__input_dillers_company form_require">
			</label>
			<label class="form__label form__label_dillers">
				<span class="form__label-text form__label-text_require">Телефон:</span>
				<input type="text" class="form__input form__input_dillers form__input_dillers_phone form_require">
			</label>
			<label class="form__label form__label_dillers">
				<span class="form__label-text">E-mail:</span>
				<input type="text" class="form__input form__input_dillers form__input_dillers_email">
			</label>
			<label class="form__label form__label_dillers">
				<span class="form__label-text form__label-text_require">Город:</span>
				<input type="text" class="form__input form__input_dillers form__input_dillers_city form_require">
			</label>
			<div class="form__label form__label_dillers"></div>
			<label class="form__label form__label_dillers-wide">
				<span class="form__label-text">Текст сообщения:</span>
				<textarea class="form__textarea form__textarea_dillers form__input_dillers_message"></textarea>
			</label>
			<label class="form__label form__label_wide">
				<span class="form__label-text form__label-text_require">Код на картинке:</span>
				<div class="form__captcha form__captcha_dillers">
					<img src="<?php echo get_template_directory_uri() . '/captcha/captcha.php' ?>" class="form__captcha-img">
				</div>
				<input type="text" class="form__input form__input_dillers-input form__input_dillers_code form_require">
			</label>
			<div class="form__button-cont-dillers">
				<button class="form__button form__button_dillers">
					<span>ОТПРАВИТЬ ЗАЯВКУ</span>
				</button>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>