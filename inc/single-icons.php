<?php
	$id 			= get_the_ID();

	$icons 			= get_post_meta($id, 'icons', true);
	$icons_wallet 	= get_post_meta($id, 'icons', true);
	$icons_sun 		= get_post_meta($id, 'icons_sun', true);
	$icons_cloud 	= get_post_meta($id, 'icons_cloud', true);
	$icons_snow 	= get_post_meta($id, 'icons_snow', true);
	$icons_temp 	= get_post_meta($id, 'icons_temp', true);
	$icons_fire 	= get_post_meta($id, 'icons_fire', true);
	$icons_eco 		= get_post_meta($id, 'icons_eco', true);
	$icons_sound 	= get_post_meta($id, 'icons_sound', true);
	$icons_color 	= get_post_meta($id, 'icons_color', true);
	$icons_streight = get_post_meta($id, 'icons_streight', true);
?>

<?php if ($icons): ?>
	<div class="product__head-icons">
		<?php if ($icons_wallet): ?>
			<div class="product__head-icon product__head-icon_wallet">
				<div class="product__head-icon-text">Доступная цена</div>
			</div>
		<?php endif; ?>

		<?php if ($icons_sun): ?>
			<div class="product__head-icon product__head-icon_sun">
				<div class="product__head-icon-text">УФ защита</div>
			</div>
		<?php endif; ?>

		<?php if ($icons_cloud): ?>
			<div class="product__head-icon product__head-icon_cloud">
				<div class="product__head-icon-text">Влагостойкость</div>
			</div>
		<?php endif; ?>
		
		<?php if ($icons_snow): ?>
			<div class="product__head-icon product__head-icon_snow">
				<div class="product__head-icon-text">Морозостойкость</div>
			</div>
		<?php endif; ?>
		
		<?php if ($icons_temp): ?>
			<div class="product__head-icon product__head-icon_temp">
				<div class="product__head-icon-text">Устойчивость к колебаниям t</div>
			</div>
		<?php endif; ?>
		
		<?php if ($icons_fire): ?>
			<div class="product__head-icon product__head-icon_fire">
				<div class="product__head-icon-text">Высокая пожаростойкость</div>
			</div>
		<?php endif; ?>
		
		<?php if ($icons_eco): ?>
			<div class="product__head-icon product__head-icon_eco">
				<div class="product__head-icon-text">Экологичный материал</div>
			</div>
		<?php endif; ?>
		
		<?php if ($icons_sound): ?>
			<div class="product__head-icon product__head-icon_sound">
				<div class="product__head-icon-text">Шумоизоляция</div>
			</div>
		<?php endif; ?>
		
		<?php if ($icons_color): ?>
			<div class="product__head-icon product__head-icon_color">
				<div class="product__head-icon-text">Цветовое разнообразие</div>
			</div>
		<?php endif; ?>
		
		<?php if ($icons_streight): ?>
			<div class="product__head-icon product__head-icon_streight">
				<div class="product__head-icon-text">Высокая прочность</div>
			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>