<?php
	$id 			= $post->ID;
	$tech 			= get_post_meta($id, 'tech', true);
	$tech_p 		= get_post_meta($id, 'tech_p', true);
	$tech_l 		= get_post_meta($id, 'tech_l', true);

	$tech_p_name 	= get_post_meta($id, 'tech_p_name', true);
	$tech_p_value 	= get_post_meta($id, 'tech_p_value', true);

	$tech_l_name 	= get_post_meta($id, 'tech_l_name', true);
	$tech_l_value 	= get_post_meta($id, 'tech_l_value', true);
?>

<div class="metabox__tab">
	<input type="hidden" name="extra[tech]" value="">
	<input type="checkbox" id="tech-tab" name="extra[tech]" value="1" <?php if ($tech) echo 'checked'; ?>>
	<label for="tech-tab">Техническая информация</label>
	<div class="metabox__tab-inner tech">
		<div>
			<input type="hidden" name="extra[tech_p]" value="">
			<input type="checkbox" id="tech_p-tab" name="extra[tech_p]" value="1" <?php if ($tech_p) echo 'checked'; ?>>
			<label for="tech_p-tab">Технические параметры</label>
			<div class="tech__table">
				<?php
					if ($tech_p_name && $tech_p_value) {
						foreach ($tech_p_name as $k => $v) {
							?>
								<div class="tech_p">
									<label class="metabox__label">
										Название технического параметра
										<textarea name="extra[tech_p_name][]"><?php echo $tech_p_name[$k]; ?></textarea>
									</label>
									<label class="metabox__label">
										Значение технического параметра
										<textarea name="extra[tech_p_value][]"><?php echo $tech_p_value[$k]; ?></textarea>
									</label>
									<button type="button" class="button button_remove-tech-p">Удалить технический параметр</button>
								</div>
							<?php
						}
					} else {
						?>
							<div class="tech_p">
								<label class="metabox__label">
									Название логистического параметра
									<textarea name="extra[tech_p_name][]"></textarea>
								</label>
								<label class="metabox__label">
									Значение логистического параметра
									<textarea name="extra[tech_p_value][]"></textarea>
								</label>
								<button type="button" class="button button_remove-tech-p">Удалить технический параметр</button>
							</div>
						<?php
					}
				?>
				<div class="button-cont">
					<button type="button" class="button button_add-tech-p">Добавить технический параметр</button>
				</div>
			</div>
		</div>
		<div>
			<input type="hidden" name="extra[tech_l]" value="">
			<input type="checkbox" id="tech_l-tab" name="extra[tech_l]" value="1" <?php if ($tech_l) echo 'checked'; ?>>
			<label for="tech_l-tab">Логистические параметры</label>
			<div class="tech__table">
				<?php
					if ($tech_l_name && $tech_l_value) {
						foreach ($tech_l_name as $k => $v) {
							?>
								<div class="tech_l">
									<label class="metabox__label">
										Название логистического параметра
										<textarea name="extra[tech_l_name][]"><?php echo $tech_l_name[$k]; ?></textarea>
									</label>
									<label class="metabox__label">
										Значение логистического параметра
										<textarea name="extra[tech_l_value][]"><?php echo $tech_l_value[$k]; ?></textarea>
									</label>
									<button type="button" class="button button_remove-tech-l">Удалить логистический параметр</button>
								</div>
							<?php
						}
					} else {
						?>
							<div class="tech_l">
								<label class="metabox__label">
									Название логистического параметра
									<textarea name="extra[tech_l_name][]"></textarea>
								</label>
								<label class="metabox__label">
									Значение логистического параметра
									<textarea name="extra[tech_l_value][]"></textarea>
								</label>
								<button type="button" class="button button_remove-tech-l">Удалить логистический параметр</button>
							</div>
						<?php
					}
				?>
				<div class="button-cont">
					<button type="button" class="button button_add-tech-l">Добавить логистический параметр</button>
				</div>
			</div>
		</div>
	</div>
</div>