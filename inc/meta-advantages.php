<?php
	$id 			= $post->ID;

	$adv 				= get_post_meta($id, 'adv', true);
	$adv_streight 		= get_post_meta($id, 'adv_streight', true);
	$adv_simple 		= get_post_meta($id, 'adv_simple', true);
	$adv_color 			= get_post_meta($id, 'adv_color', true);
	$adv_cor_streight 	= get_post_meta($id, 'adv_cor_streight', true);
	$adv_wallet 		= get_post_meta($id, 'adv_wallet', true);
	$adv_ten 			= get_post_meta($id, 'adv_ten', true);
	$adv_fifteen 		= get_post_meta($id, 'adv_fifteen', true);
	$adv_fire 			= get_post_meta($id, 'adv_fire', true);
?>

<div class="metabox__tab">
	<input type="hidden" name="extra[adv]" value="">
	<input type="checkbox" id="adv-tab" name="extra[adv]" value="1" <?php if ($adv) echo 'checked'; ?>>
	<label for="adv-tab">Приемущества</label>
	<div class="metabox__tab-inner adv">
		<label class="metabox__label">
			<input type="hidden" name="extra[adv_streight]" value="">
			<input type="checkbox" name="extra[adv_streight]" value="1" <?php if ($adv_streight) echo 'checked' ?>>
			Высокая износостойкость
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[adv_simple]" value="">
			<input type="checkbox" name="extra[adv_simple]" value="1" <?php if ($adv_simple) echo 'checked' ?>>
			Простота монтажных работ
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[adv_color]" value="">
			<input type="checkbox" name="extra[adv_color]" value="1" <?php if ($adv_color) echo 'checked' ?>>
			Цветовое разнообразие
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[adv_cor_streight]" value="">
			<input type="checkbox" name="extra[adv_cor_streight]" value="1" <?php if ($adv_cor_streight) echo 'checked' ?>>
			Высокая коррозионная стойкость
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[adv_wallet]" value="">
			<input type="checkbox" name="extra[adv_wallet]" value="1" <?php if ($adv_wallet) echo 'checked' ?>>
			Доступная цена
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[adv_ten]" value="">
			<input type="checkbox" name="extra[adv_ten]" value="1" <?php if ($adv_ten) echo 'checked' ?>>
			Гарантийный срок службы 15 лет
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[adv_fifteen]" value="">
			<input type="checkbox" name="extra[adv_fifteen]" value="1" <?php if ($adv_fifteen) echo 'checked' ?>>
			Гарантийный срок службы 10 лет
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[adv_fire]" value="">
			<input type="checkbox" name="extra[adv_fire]" value="1" <?php if ($adv_fire) echo 'checked' ?>>
			Высокая пожаростойкость
		</label>
	</div>
</div>