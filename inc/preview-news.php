<div class="preview">
	<a href="<?php the_permalink(); ?>" class="preview__inner" style="background-image: url(<? echo get_post_meta($post->ID, 'thumb', true); ?>);"></a>
	<div class="preview__desc preview__desc_news">
		<h2 class="preview__title">
			<?php the_title(); ?>
		</h2>
		<p class="preview__about">
			<?php echo strip_tags($post->post_content); ?>
		</p>
		<a href="<?php the_permalink(); ?>" class="preview__more">Подробнее</a>
	</div>
</div>