<div class="buttons">
	<div class="buttons__line">
		<button class="buttons__button buttons__button_calc"></button>
		<div class="buttons__text">ЗАЯВКА НА РАСЧЁТ</div>
	</div>
	<div class="buttons__line">
		<button class="buttons__button buttons__button_price"></button>
		<div class="buttons__text">СКАЧАТЬ ПРАЙС</div>
	</div>
	<div class="buttons__line">
		<button class="buttons__button buttons__button_partner"></button>
		<div class="buttons__text">НАПИСАТЬ ПИСЬМО</div>
	</div>
</div>