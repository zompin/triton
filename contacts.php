<?php /*Template name: Контакты*/ ?>
<?php get_header(); ?>

<?php
	$division_name = 'Контакты';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<div id="map"></div>
<main class="wrapper">

	<aside class="aside"></aside>
	<div class="content content_company">
		<?php
			the_post();
			the_content();
		?>
	</div>
</main>

<?php get_footer(); ?>