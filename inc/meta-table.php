<?php
	$id 			= $post->ID;
	$table 			= get_post_meta($id, 'table', true) * 1;
	$table_title 	= get_post_meta($id, 'table_title', true);
	$table_content 	= get_post_meta($id, 'table_content', true);
?>
<div class="metabox__tab">
	<input type="hidden" name="extra[table]" value="">
	<input id="table" type="checkbox" name="extra[table]" value="1" <?php if ($table) echo 'checked' ?>>
	<label for="table">Таблица</label>
	<div class="metabox__tab-inner table">
		<?php
			if ($table_title && $table_content) {
				foreach ($table_title as $k => $v) {
					?>
						<div class="table__ceil">
							<label class="metabox__label">
								Заголовок ячейки
								<textarea name="extra[table_title][]"><?php echo $table_title[$k]; ?></textarea>
							</label>
							<label class="metabox__label">
								Содержимое ячейки
								<textarea name="extra[table_content][]"><?php echo $table_content[$k]; ?></textarea>
							</label>
							<button type="button" class="button button_remove-ceil">Удалить ячейку</button>
						</div>
					<?php
				}
			} else {
				?>
					<div class="table__ceil">
						<label class="metabox__label">
							Заголовок ячейки
							<textarea name="extra[table_title][]"></textarea>
						</label>
						<label class="metabox__label">
							Содержимое ячейки
							<textarea name="extra[table_content][]"></textarea>
						</label>
						<button type="button" class="button button_remove-ceil">Удалить ячейку</button>
					</div>
				<?php
			}
		?>
		<div class="button-cont">
			<button type="button" class="button button_add-ceil">Добавить ячейку</button>
		</div>
	</div>
</div>