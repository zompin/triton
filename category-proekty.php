<?php get_header(); ?>

<main>
	<?php
		$division_name = 'Проекты';
	?>
	<?php get_template_part('inc/breadcrumbs'); ?>

	<div class="previews previews-project-category">
		<?php
			if(have_posts()) {
				while (have_posts()) {
					the_post();
					get_template_part('inc/preview-project-category');
				}
			}
		?>
	</div>
</main>

<?php get_footer(); ?>