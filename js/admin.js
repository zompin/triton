;(function($) {

	$(document).ready(function() {
		var techRow = $('.table__row').eq(0).clone();
		var techTable = $('.table__item').eq(0).clone();
		var certItem = $('.cert__item').eq(0).clone();
		var ceil = $('.table__ceil').eq(0).clone();
		var productPhoto = $('.product-photo__photo').eq(0).clone();
		var elemItem = $('.elem__item').eq(0).clone();
		var oblits = $('.oblits__item').eq(0).clone();
		var install = $('.install__item').eq(0).clone();
		var docsInstall = $('.docs__item-install').eq(0).clone();
		var docsBro = $('.docs__item-bro').eq(0).clone();
		var vodostok = $('.vodostok__elem').eq(0).clone();
		var techP = $('.tech_p').eq(0).clone();
		var techL = $('.tech_l').eq(0).clone();
		var komplekt = $('.komplekt__item').eq(0).clone();
		var colorItem = $('<div class="color__item"><input type="hidden" name="extra[color_item_type][]" value=""><label class="metabox__label">Изображение цвета<textarea name="extra[color_thumb][]"></textarea></label><label class="metabox__label">Название цвета<textarea name="extra[color_name][]"></textarea></label><label class="metabox__label">Код цвета<textarea name="extra[color_code][]"></textarea></label><select name="extra[color_type][]"><option value="0">Светлая надпись</option><option value="1">Темная надпись</option></select><button type="button" class="button button_remove-color">Удалить цвет</button></div>').eq(0).clone();
		var colorHeader = $('<div class="color__item_text"><input type="hidden" name="extra[color_item_type][]" value="1"><input type="hidden" name="extra[color_thumb][]" value=""><input type="hidden" name="extra[color_code][]" value=""><input type="hidden" name="extra[color_type][]" value=""><label class="metabox__label">Заголовок<textarea name="extra[color_name][]"></textarea></label><button type="button" class="button button_remove-color">Удалить заголовок</button></div>').eq(0).clone();

		$(colorItem).find('input, textarea').val('');
		$(techRow).find('input, textarea').val('');
		$(techTable).find('input, textarea').val('');
		$(certItem).find('input, textarea').val('');
		$(ceil).find('input, textarea').val('');
		$(productPhoto).find('input, textarea').val('');
		$(elemItem).find('input, textarea').val('');
		$(oblits).find('input, textarea').val('');
		$(install).find('input, textarea').val('');
		$(docsInstall).find('input, textarea').val('');
		$(docsBro).find('input, textarea').val('');
		$(vodostok).find('input, textarea').val('');
		$(techP).find('input, textarea').val('');
		$(techL).find('input, textarea').val('');
		$(komplekt).find('input, textarea').val('');

		// Цвет

		$('.color').on('click', '.button_add-color', function() {
			$('.color__button-cont').before($(colorItem).clone());
		}).on('click', '.button_remove-color', function() {
			$(this).parent().remove();
		}).on('click', '.button_add-header-color', function() {
			$('.color__button-cont').before($(colorHeader).clone());
		});

		// Цвет

		// Техническая информация

		$('.tech').on('click', '.button_add-tech-p', function() {
			$(this).parent().before($(techP).clone());
		}).on('click', '.button_remove-tech-p', function() {
			$(this).parent().remove();
		}).on('click', '.button_add-tech-l', function() {
			console.log('string')
			$(this).parent().before($(techL).clone());
		}).on('click', '.button_remove-tech-l', function() {
			$(this).parent().remove();
		});

		// Техническая информация

		// Сертификаты

		$('.cert').on('click', '.button_add-cert', function() {
			$('.cert__button-cont').before($(certItem).clone());
		}).on('click', '.button_cert-remove', function() {
			$(this).parent().remove();
		});

		// Сертификаты

		// Таблица

		$('.table').on('click', '.button_add-ceil', function() {
			$(this).parent().before($(ceil).clone());
		}).on('click', '.button_remove-ceil', function() {
			$(this).parent().remove();
		});

		// Таблица

		// Фото продукта

		$('.product-photo').on('click', '.button_remove-product-photo', function() {
			$(this).parent().remove();
		}).on('click', '.button_add-product-photo', function() {
			$(this).parent().before($(productPhoto).clone());
		});

		// Фото продукта

		// Доборны элементы

		$('.elem').on('click', '.button_add-elem', function() {
			$(this).parent().before($(elemItem).clone());
		}).on('click', '.button_remove-elem', function() {
			$(this).parent().remove();
		});

		// Доборны элементы

		// Облицовка

		$('.oblits').on('click', '.button_add-oblits', function() {
			$(this).parent().before($(oblits).clone());
		}).on('click', '.button_remove-oblits', function() {
			$(this).parent().remove();
		});

		// Облицовка

		// Монтаж

		$('.install').on('click', '.button_add-install', function() {
			$(this).parent().before($(install).clone());
		}).on('click', '.button_remove-install', function() {
			$(this).parent().remove();
		});

		// Монтаж

		// Документы

		$('.docs_install').on('click', '.button_docs-install-add', function() {
			$(this).parent().before($(docsInstall).clone());
		}).on('click', '.button_docs-install-remove', function() {
			$(this).parent().remove();
		});

		$('.docs_bro').on('click', '.button_docs-bro-add', function() {
			$(this).parent().before($(docsBro).clone());
		}).on('click', '.button_docs-bro-remove', function() {
			$(this).parent().remove();
		});

		// Документы

		// Элементы водостока

		$('.vodostok').on('click', '.button_add-vodostok', function() {
			$(this).parent().before($(vodostok).clone());
		}).on('click', '.button_remove-vodostok', function() {
			$(this).parent().remove();
		});

		// Элементы водостока

		// Комплект

		$('.komplekt').on('click', '.button_add-komplekt', function() {
			$(this).parent().before($(komplekt).clone());
		}).on('click', '.button_remove-komplekt', function() {
			$(this).parent().remove();
		});

		// Комплект

		$('.metabox__tab-inner').sortable({
			cancel: '.color__button-cont, textarea, input'
		});
	});
})(jQuery);