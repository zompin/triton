<?php
	$id = $post->ID;

	$projects = get_post_meta($id, 'projects', true);
	$projects_cats = get_categories(array('child_of' => 7));
	$projects_cat = get_post_meta($id, 'projects_cat', true);
?>

<div class="metabox__tab projects">
	<input type="hidden" name="extra[projects]" value="">
	<input type="checkbox" id="projects-tab" name="extra[projects]" value="1" <?php if ($projects) echo 'checked'; ?>>
	<label for="projects-tab">Проекты</label>
	<div class="metabox__tab-inner">
		<?php
			if ($projects_cats)  {
				foreach ($projects_cats as $cat) {
					?>
						<label class="metabox__label">
							<input name="extra[projects_cat]" value="<?php echo $cat->cat_ID; ?>" type="radio" <?php if ($projects_cat == $cat->cat_ID) echo 'checked'; ?>>
							<?php echo $cat->name; ?>
						</label>
					<?php
				}
			}
		?>
	</div>
</div>