<?php
	$id = $post->ID;
	$part_desc = get_post_meta($id, 'part_desc', true);
?>

<div class="metabox__tab">
	<label class="metabox__label">
		Краткое описание
		<textarea name="extra[part_desc]"><?php echo $part_desc; ?></textarea>
	</label>
</div>