;(function($) {
	var formUrl = '/wp-content/themes/triton/send.php';

	// Search
	$(document).on('click', '.header__show-search-button', function() {
		$('.header__search-cont').toggle();
	}).on('click', 'body', function(e) {
		if ($(e.target).closest('.header__search-cont').length == 0 && $(e.target).closest('.header__red-inner').length == 0) {
			$('.header__search-cont').hide();
		}
	});


	// Buttons
	$('.buttons__button_calc').click(function() {
		$('.form__fixed_calc').show();
		bodyScrollToggle();
	});

	$('.buttons__button_price').click(function() {
		location.href = '/prais';
	});

	$('.buttons__button_partner').click(function() {
		$('.form__fixed_partner').show();
		bodyScrollToggle();
	});

	// Изображения
	$('.product__head-photos-item img').mouseenter(showImage);
	$('.product__head-photos-item img').click(showImage);

	function showImage() {
		var src = $(this).attr('src');

		$('.product__head-photos-item').removeClass('product__head-photos-item_current');
		$(this).parent().parent().addClass('product__head-photos-item_current');
		$('.product__head-thumb').css('backgroundImage', 'url(' + src + ')');
	}

	$('.product__head-photos').mouseleave(function() {
		var src = $('.product__head-photos-item').eq(0).find('img').attr('src');
		$('.product__head-thumb').css('backgroundImage', 'url(' + src + ')');
		$('.product__head-photos-item').removeClass('product__head-photos-item_current');
		$('.product__head-photos-item').eq(0).addClass('product__head-photos-item_current');
	});

	// Forms
	$('.form__close, .form__close-div').click(function() {
		$(this).closest('.form__fixed').hide();
		bodyScrollToggle();
	});

	$('.form__file_calc').change(function() {
		var files = [];

		for (var i = 0; i < this.files.length; i++) {
			files.push(this.files[i].name);
		}

		files = files.join(', ');
		$('.form__file-files').attr('data-files', files);
	});

	$('.form__button-remove').click(function() {
		$('.form__file').val('');
		$('.form__file-files').attr('data-files', '');
	});

	$('.button-border_question-show-form').click(function() {
		$('.wrapper-question_form').toggle();
	});

	$('.form__button_show-calc-form').click(function() {
		$(this).hide();
		$('.form_calc-product').show();
	});

	$('.form__button_calc').click(function() {
		var checked = true;
		var fd = new FormData();
		var files = $('.form__file_calc_file')[0].files;

		var name = $(this).closest('.form').find('.form__input_calc-name').val();
		var email = $(this).closest('.form').find('.form__input_calc-email').val();
		var phone = $(this).closest('.form').find('.form__input_calc-phone').val();
		var material = $(this).closest('.form').find('.form__textarea_calc_material').val();
		var code = $(this).closest('.form').find('.form__input_captcha-calc').val();
		var type = 'calc';

		fd.append('name', name);
		fd.append('email', email);
		fd.append('phone', phone);
		fd.append('material', material);
		fd.append('code', code);
		fd.append('type', type);

		$('.form_calc .form_require').each(function(i, e) {
			if ($(e).val().length == 0) {
				$(e).addClass('form_error');
				checked = false;
			} else {
				$(e).removeClass('form_error');
			}
		});

		if (files.length) {
			fd.append('filesCount', files.length);

			for (var i = 0; i < files.length; i++) {
				fd.append('file' + i, files[i]);
			}
		}

		ajax(fd, true, this, checked, 'Ваша заявка отправлена');
	});

	$('.form__button_price').click(function() {
		var data = {};
		var checked = true;
		data.name = $('.form__input_price-name').val();
		data.phone = $('.form__input_price-phone').val();
		data.email = $('.form__input_price-email').val();
		data.code = $('.form__input_price-captcha').val();
		data.type = 'price';

		$('.form_price .form__input').each(function(i, e) {
			if ($(e).val().length == 0) {
				$(e).addClass('form_error');
				checked = false;
			} else {
				$(e).removeClass('form_error');
			}
		});

		ajax(data, false, this, checked, 'Ваша заявка отправлена', function() {
			var price = $('#price').find('a');

			if (price.length) {
				price = price.attr('href');

				if (price) {
					location.href = price;
				}
			}
		});
	});

	$('.form__button_partner').click(function() {
		var data = {};
		var checked = true;

		data.name = $('.form__input_partner-name').val();
		data.contact = $('.form__input_partner-phone').val();
		data.message = $('.form__input_partner-message').val();
		data.code = $('.form__input_partner-captcha').val();
		data.type = 'message';


		$('.form_partner .form_require').each(function(i ,e) {
			if ($(e).val().length == 0) {
				checked = false;
				$(e).addClass('form_error');
			} else {
				$(e).removeClass('form_error');
			}
		});

		ajax(data, false, this, checked, 'Ваша заявка отправлена');
	});

	$('.button-bordered_question').click(function() {
		var data = {};
		var checked = true;

		data.name = $('.form__input_question-name').val();
		data.email = $('.form__input_question-email').val();
		data.phone = $('.form__input_question-phone').val();
		data.question = $('.form__input_question-question').val();
		data.code = $('.form__input_captcha-question').val();
		data.type = 'qeustion';

		$('.form_question .form_require').each(function(i, e) {
			if ($(e).val().length == 0) {
				checked = false
				$(e).addClass('form_error');
			} else {
				$(e).removeClass('form_error');
			}
		});

		ajax(data, false, this, checked, 'Ваш вопрос отправлен');
	});

	$('.form__button_dillers').click(function() {
		var data = {};
		var checked = true;
		var name = $('.form__input_dillers_name').val();
		var company = $('.form__input_dillers_company').val();
		var phone = $('.form__input_dillers_phone').val();
		var email = $('.form__input_dillers_email').val();
		var city = $('.form__input_dillers_city').val();
		var message = $('.form__input_dillers_message').val();
		var code = $('.form__input_dillers_code').val();

		data.name = name;
		data.company = company;
		data.phone = phone;
		data.email = email;
		data.city = email;
		data.message = message;
		data.code = code;
		data.type = 'dillers';

		$('.form_dillers .form_require').each(function(i, e) {
			if ($(e).val().length == 0) {
				checked = false
				$(e).addClass('form_error');
			} else {
				$(e).removeClass('form_error');
			}
		});

		ajax(data, false, this, checked, 'Ваша заявка отправлена');
	});

	$('.button_send-cv').click(function() {
		var fd = new FormData();
		var checked = true;

		var lname = $('.form__input_cv-lname').val();
		var fname = $('.form__input_cv-fname').val();
		var bday = $('.form__input_cv-day').val();
		var bmonth = $('.form__input_cv-month').val();
		var byear = $('.form__input_cv-year').val();
		var area = $('.form__input_cv-area').val();
		var city = $('.form__input_cv-city').val();
		var street = $('.form__input_cv-street').val();
		var home = $('.form__input_cv-home').val();
		var q = $('.form__input_cv-q').val();
		var phone = $('.form__input_cv-phone').val();
		var email = $('.form__input_cv-email').val();
		var status = $('.form__input_cv-status:checked').val();
		var kids = $('.form__input_cv-kids:checked').val();
		var edType = $('.form__input_cv-educ-type:checked').val();
		var univ = $('.form__input_cv-univ').val();
		var spec = $('.form__input_cv-spec').val();
		var comp = $('.form__input_comp:checked').val();
		var kz = $('.form__input-lang-kz').prop('checked') * 1;
		var ru = $('.form__input-lang-ru').prop('checked') * 1;
		var en = $('.form__input-lang-en').prop('checked') * 1;
		var other = $('.form__input-lang-other').prop('checked') * 1;
		var otherlang = $('.form__input_lang-other-name').val();
		var drive = $('.form__input_drive:checked').val();
		var priv = $('.form__input_cv-priv').val();
		var hob = $('.form__input_cv-hob').val();
		var specArea = $('.form__input_cv-cpec-area').val();
		var fplace = $('.form__input_cv-fplace').val();
		var pay = $('.form__input_cv-pay').val();
		var code = $('.form__input_captcha_cv').val();
		var file = $('.form__file-photo-file')[0].files;

		fd.append('lname', lname);
		fd.append('fname', fname);
		fd.append('bday', bday);
		fd.append('bmonth', bmonth);
		fd.append('byear', byear);
		fd.append('area', area);
		fd.append('city', city);
		fd.append('street', street);
		fd.append('home', home);
		fd.append('q', q);
		fd.append('phone', phone);
		fd.append('email', email);
		fd.append('status', status);
		fd.append('kids', kids);
		fd.append('edType', edType);
		fd.append('univ', univ);
		fd.append('spec', spec);
		fd.append('comp', comp);
		fd.append('kz', kz);
		fd.append('ru', ru);
		fd.append('en', en);
		fd.append('type', 'cv');

		if (other) fd.append('otherlang', otherlang);

		fd.append('drive', drive);
		fd.append('priv', priv);
		fd.append('hob', hob);
		fd.append('specArea', specArea);
		fd.append('fplace', fplace);
		fd.append('pay', pay);
		fd.append('code', code);

		$('.form__expirience').each(function(i, e) {
			var period = $(e).find('.form__input_cv-period').val();
			var company = $(e).find('.form__input_cv-company').val();
			var location = $(e).find('.form__input_cv-location').val();
			var area = $(e).find('.form__input_cv-company-area').val();
			var place = $(e).find('.form__input_cv-place').val();
			var data = {};

			data.period = period;
			data.company = company;
			data.location = location;
			data.area = area;
			data.place = place;

			fd.append('exp[]', JSON.stringify(data));
		});


		if (file) {
			fd.append('photo', file[0]);
		}

		$('.form_cv .form_require').each(function(i, e) {
			if ($(e).val().length == 0) {
				checked = false
				$(e).addClass('form_error');
			} else {
				$(e).removeClass('form_error');
			}
		});

		ajax(fd, true, this, checked, 'Ваше резюме отправлено');
	});

	$('.button-border_fill').click(function() {
		$('.wrapper-cv').toggle();
	});

	$('.form__add-cv').click(function() {
		var exp = $('.form__expirience').eq(0).clone();
		exp.find('input').val('');

		$(this).parent().before(exp);
	});

	$(document).on('click', '.form__expririence-remove', function(e) {
		$(e.target).parent().remove();
	});

	function ajax(data, isFd, button, checked, message, callback) {
		var sendObj = {
			type: 'POST',
			url: formUrl,
			data: data,
			cache: false,
			success: function(res) {
				$(button).prop('disabled', false);

				if (res == '1') {
					alert(message);
					if (callback) {
						callback();
					}
				} else if (res == '0') {
					alert('Не удалось отправить запрос');
				} else {
					alert(res);
				}
			},
			error: function() {
				$(button).prop('disabled', false);
				alert('Произошла ошибка.');
			}
		}

		if (isFd) {
			sendObj.contentType = false;
			sendObj.processData = false;
		}

		if (!checked) {
			alert('Вы не заполнини обязательные поля');
		} else {

			$(button).prop('disabled', true);

			$.ajax(sendObj);
		}
	}

	function bodyScrollToggle() {
		var wrapper = $('#wrapper');

		if (wrapper.hasClass('hidden')) {
			wrapper.css('overflow', 'none');
			wrapper.removeClass('hidden');
		} else {
			wrapper.css('overflow', 'hidden');
			wrapper.addClass('hidden');
		}
	}


	$('.slider__items').slick({
		arrows: false,
		dots: true,
		appendDots: '.slider__control',
		autoplay: true
	});

	$('.product__komplekt-items').slick({
		prevArrow: '.product__komplekt-button_prev',
		nextArrow: '.product__komplekt-button_next',
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [{
			breakpoint: 700,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			}
		}, {
			breakpoint: 551,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		}, {
			breakpoint: 431,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});

	// Вкладки

	$('.product__tabs-title').eq(0).addClass('product__tabs-title_current');
	$('.product__tabs').show();
	$('.product__tab').eq(0).show();
	$('.product__tabs-title').click(function() {
		var tab = $(this).attr('data-tab');

		$('.product__tabs-title').removeClass('product__tabs-title_current');
		$(this).addClass('product__tabs-title_current');
		$('.product__tab').hide();
		$('.product__tab_' + tab).show();
	});

	// Вкладки

	// Меню

	$('.header__menu-button').click(function() {
		var wrapper = $('#wrapper');
		//$('.adaptive-menu__cont').toggle();

		/*if (wrapper.hasClass('overflow-x')) {
			wrapper.css('overflow-x', 'auto');
		} else {
			wrapper.css('overflow-x', 'hidden');
		}*/

		wrapper.toggleClass('overflow-x');
		$('#wrapper').toggleClass('adaptive_show');
	});

	$('.adaptive-menu__cont .menu-item-has-children > a').click(function(e) {
		e.preventDefault();

		$(this).toggleClass('show');
		$(this).parent().find('.sub-menu').toggle();
	});

	$('.aside__menu').click(function(e) {
		if (e.target.nodeName == 'BUTTON') {
			$(e.target).parent().parent().toggleClass('hide');
			return false;
		}
	});

	// Меню


	// Вопрос-ответ

	$('.questions__all').click(function(e) {
		e.preventDefault();
		var mode = $(this).attr('data-state');
		var text;

		if (mode == 'show') {
			mode = 'hide';
			text = $(this).attr('data-hide');
			$(this).next().find('.questions__item-desc').show();
			$(this).addClass('arrow-open');
		} else {
			mode = 'show';
			text = $(this).attr('data-show');
			$(this).next().find('.questions__item-desc').hide();
			$(this).removeClass('arrow-open');
		}

		$(this).attr('data-state', mode);
		$(this).html(text);

	});

	$('.questions__item-header').click(function(e) {
		e.preventDefault();
		$(this).toggleClass('arrow-open');
		$(this).next().toggle();
	});

	// Вопрос-ответ

	$('.popup__close, .popup').click(function(e) {
		if (e.target.nodeName == 'IMG' || $(e.target).hasClass('preview__edit')) return;
		$('.popup').removeClass('popup_show');
		$('.popup__img').attr('src', '');
		bodyScrollToggle();
	});

	$('.preview__inner_project-category, .product__projects-item').click(function(e) {
		var img = $(this).css('backgroundImage');

		if ($(e.target).hasClass('preview__edit')) {
			return;
		}

		img = img.substring(img.indexOf('"') + 1, img.lastIndexOf('"'));

		$('.popup__img').attr('src', img);
		$('.popup').addClass('popup_show');
		bodyScrollToggle();
	});

	/*$('.product__projects-item').click(function() {
		var img = $(this).find('img').attr('src');

		$('.popup__img').attr('src', img);
		$('.popup').addClass('popup_show');
		bodyScrollToggle();
	});*/

	$('.cert__item-image img').click(function() {
		var img = $(this).attr('src');

		$('.popup__img').attr('src', img);
		$('.popup').addClass('popup_show');
		bodyScrollToggle();
	});

	$('.to-top').click(function() {
		$('html, body').stop().animate({
			scrollTop: 0,
		},
		500,
		'swing',
		function() {
			$('body, html').scrollTop(0);
		});
		return false;
	});

	$('.preview__about').dotdotdot({ellipsis: '...'});
	$('.product__head-text').dotdotdot({ellipsis: '...'});

	$(document).on('scroll', showToTop);
	$(document).on('touchmove', showToTop);

	function showToTop() {
		var offset = $('body').scrollTop() ||  $('html').scrollTop();
		var height = $(document).height();
		//offset += $(window).height();

		if (offset > 500) {
			$('.to-top').addClass('to-top_show');
		} else {
			$('.to-top').removeClass('to-top_show');
		}
	}

	;(function() {
		var page = location.pathname.replace(/\//g, '');
		
		if (page == 'prais') {
			$('.content a').click(function(e) {
				e.preventDefault();
				$('.form__fixed_price').show();
				bodyScrollToggle();
			});
		}
	})();

	/*if (isTouch()) {
		$('.preview').addClass('preview_touch');
	}

	function isTouch() {
		return !!('ontouchstart' in window || navigator.maxTouchPoints);
	}*/
})(jQuery);