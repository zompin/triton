<?php
	$id = $post->ID;
	$thumb = get_post_meta($id, 'thumb', true);
?>

<div class="metabox__tab">
	<label class="metabox__label">
		Превью
		<textarea name="extra[thumb]"><?php echo $thumb; ?></textarea>
	</label>
</div>