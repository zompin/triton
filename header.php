<!DOCTYPE html>
<html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/imgs/favicon.png'; ?>" type="image/png">
<?php wp_head(); ?>
</head>

<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WCBKZG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WCBKZG');</script>
<!-- End Google Tag Manager -->

	<div id="wrapper">
		<header class="header">
			<div class="header__white">
				<button class="header__menu-button">
					<span></span>
				</button>
				<div class="header__white-inner">
					<a href="/" class="header__logo">
						<img class="header__logo-img" src="<? echo get_template_directory_uri() . '/imgs/logo-header.png'; ?>" alt="">
						<img class="header__logo-text" src="<? echo get_template_directory_uri() . '/imgs/logo-header-text.png'; ?>" alt="">
					</a>
					<div class="header__contacts">
						<div class="header__contacts-item">
							<div class="header__contacts-city">Караганда</div>
							<a href="tel:+77212566235" class="header__contact">
								<span>+7</span>
								<span>/7212/</span>
								<span>566</span>
								<span>235</span>
							</a>
							<a href="tel:+77215429219" class="header__contact">
								<span>+7</span>
								<span>/72154/</span>
								<span>29</span>
								<span>219</span>
							</a>
						</div>
						<div class="header__contacts-item">
							<div class="header__contacts-city">Астана</div>
							<a href="tel:+77172493268" class="header__contact">
								<span>+7</span>
								<span>/7172/</span>
								<span>493</span>
								<span>268</span>
							</a>
							<a href="tel:+77012555957" class="header__contact">
								<span>+7</span>
								<span>701</span>
								<span>255</span>
								<span>5957</span>
							</a>
						</div>
						<div class="header__contacts-price">
							<a href="<?php echo home_url('/prais'); ?>">Скачать прайс</a>
						</div>
					</div>
				</div>
			</div>
			<div class="header__red">
				<div class="header__red-inner">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'fallback_cb' => '__return_empty_string',
								'depth' => 2,
								'container' => '',
								'menu_id' => '',
								'menu_class' => 'header__menu'
							)
						);
					?>
					<button class="header__show-search-button"></button>
				</div>
			</div>
			<div class="header__search-cont">
				<form class="header__search" action="<?php echo home_url( '/' ) ?>">
					<input type="text" class="header__search-input" name="s" placeholder="Найти">
					<button type="submit" class="header__search-submit"></button>
				</form>
			</div>
		</header>
		<div class="adaptive-menu__cont">
			<?php
				wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'fallback_cb' => '__return_empty_string',
						'depth' => 2,
						'container' => '',
						'menu_id' => '',
						'menu_class' => 'adaptive-menu'
					)
				);
			?>
		</div>