<div class="preview preview_product-main">
	<div class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_post_meta($id, 'thumb', true); ?>);">
		<a href="<?php the_permalink(); ?>" class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</a>
		<div class="preview__title-img">
			<?php the_title(); ?>
			<div class="preview__cat">
				<?php echo get_the_category()[0]->cat_name; ?>
			</div>
		</div>
	</div>
</div>