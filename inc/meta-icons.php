<?php
	$id 			= $post->ID;

	$icons 			= get_post_meta($id, 'icons', true);
	$icons_wallet 	= get_post_meta($id, 'icons_wallet', true);
	$icons_sun 		= get_post_meta($id, 'icons_sun', true);
	$icons_cloud 	= get_post_meta($id, 'icons_cloud', true);
	$icons_snow 	= get_post_meta($id, 'icons_snow', true);
	$icons_temp 	= get_post_meta($id, 'icons_temp', true);
	$icons_fire 	= get_post_meta($id, 'icons_fire', true);
	$icons_eco 		= get_post_meta($id, 'icons_eco', true);
	$icons_sound 	= get_post_meta($id, 'icons_sound', true);
	$icons_color 	= get_post_meta($id, 'icons_color', true);
	$icons_streight = get_post_meta($id, 'icons_streight', true);
?>

<div class="metabox__tab">
	<input type="hidden" name="extra[icons]" value="">
	<input type="checkbox" id="icons-tab" name="extra[icons]" value="1" <?php if ($icons) echo 'checked'; ?>>
	<label for="icons-tab">Пиктограммы</label>
	<div class="metabox__tab-inner icons">
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_wallet]" value="">
			<input type="checkbox" name="extra[icons_wallet]" value="1" <?php if ($icons_wallet) echo 'checked' ?>>
			Доступная цена
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_sun]" value="">
			<input type="checkbox" name="extra[icons_sun]" value="1" <?php if ($icons_sun) echo 'checked' ?>>
			УФ защита
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_cloud]" value="">
			<input type="checkbox" name="extra[icons_cloud]" value="1" <?php if ($icons_cloud) echo 'checked' ?>>
			Влагостойкость
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_snow]" value="">
			<input type="checkbox" name="extra[icons_snow]" value="1" <?php if ($icons_snow) echo 'checked' ?>>
			Морозостойкость
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_temp]" value="">
			<input type="checkbox" name="extra[icons_temp]" value="1" <?php if ($icons_temp) echo 'checked' ?>>
			Устойчивость к колебаниям t
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_fire]" value="">
			<input type="checkbox" name="extra[icons_fire]" value="1" <?php if ($icons_fire) echo 'checked' ?>>
			Высокая пожаростойкость
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_eco]" value="">
			<input type="checkbox" name="extra[icons_eco]" value="1" <?php if ($icons_eco) echo 'checked' ?>>
			Экологичный материал
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_sound]" value="">
			<input type="checkbox" name="extra[icons_sound]" value="1" <?php if ($icons_sound) echo 'checked' ?>>
			Шумоизоляция
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_color]" value="">
			<input type="checkbox" name="extra[icons_color]" value="1" <?php if ($icons_color) echo 'checked' ?>>
			Цветовое разнообразие
		</label>
		<label class="metabox__label">
			<input type="hidden" name="extra[icons_streight]" value="">
			<input type="checkbox" name="extra[icons_streight]" value="1" <?php if ($icons_streight) echo 'checked' ?>>
			Высокая прочность
		</label>
	</div>
</div>