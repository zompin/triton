<div class="product__tabs">
	<?php
		$id 			= get_the_ID();

		$colors 		= get_post_meta($id, 'colors', true) * 1;
		$color_thumb 	= get_post_meta($id, 'color_thumb', true);
		$color_name 	= get_post_meta($id, 'color_name', true);
		$color_code 	= get_post_meta($id, 'color_code', true);
		$color_type 	= get_post_meta($id, 'color_type', true);
		$color_text 	= get_post_meta($id, 'color_text', true);
		$color_item_type = get_post_meta($id, 'color_item_type', true);

		$tech 			= get_post_meta($id, 'tech', true);
		$tech_p 		= get_post_meta($id, 'tech_p', true);
		$tech_l 		= get_post_meta($id, 'tech_l', true);
		$tech_p_name 	= get_post_meta($id, 'tech_p_name', true);
		$tech_p_value 	= get_post_meta($id, 'tech_p_value', true);
		$tech_l_name 	= get_post_meta($id, 'tech_l_name', true);
		$tech_l_value 	= get_post_meta($id, 'tech_l_value', true);

		$elem 			= get_post_meta($id, 'elem', true);
		$elem_text 		= get_post_meta($id, 'elem_text', true);
		$elem_image 	= get_post_meta($id, 'elem_image', true);

		$oblits 		= get_post_meta($id, 'oblits', true);
		$oblits_name 	= get_post_meta($id, 'oblits_name', true);
		$oblits_image 	= get_post_meta($id, 'oblits_image', true);

		$install 		= get_post_meta($id, 'install', true);
		$install_text 	= get_post_meta($id, 'install_text', true);
		$install_name 	= get_post_meta($id, 'install_name', true);
		$install_image 	= get_post_meta($id, 'install_image', true);
		$install_desc 	= get_post_meta($id, 'install_desc', true);

		$vodostok 		= get_post_meta($id, 'vodostok', true);
		$vodostok_elem 	= get_post_meta($id, 'vodostok_elem', true);
		$vodostok_ch 	= get_post_meta($id, 'vodostok_ch', true);
		$vodostok_value = get_post_meta($id, 'vodostok_value', true);
		$vodostok_image = get_post_meta($id, 'vodostok_image', true);

		$custom 		= get_post_meta($id, 'custom', true);
		$custom_name 	= get_post_meta($id, 'custom_name', true);
		$custom_text 	= get_post_meta($id, 'custom_text', true);
	?>
	<div class="product__tabs-heads">
		<?php
			if ($colors) {
				?>
					<button class="product__tabs-title" data-tab="color">ЦВЕТ</button>
				<?php
			}

			if ($tech) {
				?>
					<button class="product__tabs-title" data-tab="tech">ТЕХНИЧЕСКАЯ ИНФОРМАЦИЯ</button>
				<?php
			}

			if ($elem) {
				?>
					<button class="product__tabs-title" data-tab="elem">ДОБОРНЫЕ ЭЛЕМЕНТЫ</button>
				<?php
			}

			if ($oblits) {
				?>
					<button class="product__tabs-title" data-tab="oblits">ВИДЫ ОБЛИЦОВОК</button>
				<?php
			}

			if ($install) {
				?>
					<button class="product__tabs-title" data-tab="install">МОНТАЖ</button>
				<?php
			}

			if ($vodostok) {
				?>
					<button class="product__tabs-title" data-tab="vodostok">ЭЛЕМЕНТЫ ВОДОСТОКА</button>
				<?php
			}

			if ($custom) {
				?>
					<button class="product__tabs-title" data-tab="custom"><?php echo $custom_name; ?></button>
				<?php
			}
		?>
	</div>
	<div class="product__tabs-items">
		<?php
			if ($colors) {
				?>
					<div class="product__tab product__tab_color">
						<div class="product__colors">
							<div class="product__colors-text">
								<?php echo $color_text; ?>
							</div>
							<div class="product__colors-items">
								<?php
									foreach ($color_thumb as $k => $v) {
										?>
											<?php if ($color_item_type[$k]): ?>
												<div class="product__colors-header">
													<?php echo $color_name[$k]; ?>
												</div>
											<?php else: ?>
												<div class="product__colors-item <?php echo $color_type[$k] ? 'product__colors-item_dark' : ''; ?>">
													<div class="product__colors-item-thumb" style="background-image: url(<?php echo $color_thumb[$k]; ?>);">
														<?php if ($color_code[$k]): ?>
															<div class="product__colors-item-code">
																<span>
																	<?php echo $color_code[$k]; ?>
																</span>
															</div>
														<?php endif;?>
													</div>
													<div class="product__colors-item-title">
														<?php echo $color_name[$k]; ?>
													</div>
												</div>
											<?php endif; ?>
										<?php
									}
								?>
							</div>
						</div>
					</div>
				<?php
			}
		?>

		<?php
			if ($tech) {
				?>
					<div class="product__tab product__tab_tech">
						<div class="product__inner-tables">
							<?php if($tech_p): ?>
								<div class="product__inner-table">
									<div class="product__inner-title">Технические параметры</div>
									<?php
										foreach ($tech_p_name as $k => $v) {
											?>
												<div class="product__inner-row">
													<div class="product__inner-ceil product__inner-ceil_name">
														<?php echo $tech_p_name[$k]; ?>
													</div>
													<div class="product__inner-ceil product__inner-ceil_value">
														<?php echo $tech_p_value[$k]; ?>
													</div>
												</div>
											<?php
										}
									?>
								</div>
							<?php endif; ?>
							<?php if($tech_l): ?>
								<div class="product__inner-table">
									<div class="product__inner-title">Логистические параметры</div>
									<?php
										foreach ($tech_l_name as $k => $v) {
											?>
												<div class="product__inner-row">
													<div class="product__inner-ceil product__inner-ceil_name">
														<?php echo $tech_l_name[$k]; ?>
													</div>
													<div class="product__inner-ceil product__inner-ceil_value">
														<?php echo $tech_l_value[$k]; ?>
													</div>
												</div>
											<?php
										}
									?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php
			}
		?>

		<?php
			if ($elem) {
				?>
					<div class="product__tab product__tab_elem">
						<div class="product__elem-text">
							<?php echo $elem_text; ?>
						</div>
						<div class="product__elem">
							<?php
								if ($elem_image) {
									echo '<img src="' . $elem_image . '">';
								}
							?>
						</div>
					</div>
				<?php
			}
		?>

		<?php
			if ($oblits) {
				?>
					<div class="product__tab product__tab_oblits">
						<div class="product__oblits">
							<div class="product__oblits-header">ВИДЫ ОБЛИЦОВОК ТРЕХСЛОЙНЫХ СЭНДВИЧ-ПАНЕЛЕЙ</div>
							<div class="product__oblits-text">Внешняя и внутренняя стороны сэндвич- панели могут быть представлены в различных вариантах с В, Т, Н – образным профилем или гладкой поверхностью.</div>
							<?php
								foreach ($oblits_image as $k => $v) {
									?>
										<div class="product__oblits-item">
											<div class="product__oblits-name">
												<?php echo $oblits_name[$k]; ?>
											</div>
											<div class="product__oblits-image">
												<img src="<?php echo $oblits_image[$k]; ?>">
											</div>
										</div>
									<?php
								}
							?>
						</div>
					</div>
				<?php
			}
		?>
		
		<?php
			if ($install) {
				?>
					<div class="product__tab product__tab_install">
						<div class="product__install">
							<div class="product__install-text"><?php echo $install_text; ?></div>
							<?php
								foreach ($install_name as $k => $v) {
									?>
										<div class="product__install-item">
											<div class="product__install-name">
												<?php echo $install_name[$k]; ?>
											</div>
											<div class="product__install-row">
												<div class="product__install-image">
													<img src="<?php echo $install_image[$k]; ?>">
												</div>
												<div class="product__install-desc">
													<?php echo $install_desc[$k]; ?>
												</div>
											</div>
										</div>
									<?php
								}
							?>
						</div>
					</div>
				<?php
			}
		?>
		<?php
			if ($vodostok) {
				?>
					<div class="product__tab product__tab_vodostok">
						<div class="product__vodostok">
							<div class="product__vodostok-table">
								<div class="product__vodostok-head">
									<div class="product__vodostok-head-ceil">Элементы</div>
									<div class="product__vodostok-head-ceil">Назначение</div>
								</div>
								<?php
									foreach ($vodostok_elem as $k => $v) {
										?>
											<div class="product__vodostok-row">
												<div class="product__vodostok-ceil">
													<span>
														<?php echo $vodostok_elem[$k]; ?>
													</span>
													<span>
														<?php echo $vodostok_ch[$k]; ?>
													</span>
												</div>
												<div class="product__vodostok-ceil">
													<?php echo $vodostok_value[$k]; ?>
												</div>
											</div>
										<?php
									}
								?>
								</div>
								<?php
									if ($vodostok_image) {
										echo '<img src="' . $vodostok_image . '">';
									}
								?>
							</div>
						</div>
					</div>
				<?php
			}
		?>
		<?php
			if ($custom) {
				?>
					<div class="product__tab product__tab_custom">
						<div class="product__custom">
							<?php echo $custom_text; ?>
						</div>
					</div>
				<?php
			}
		?>
	</div>
	<?php
		the_post();
		the_content();
	?>
</div>