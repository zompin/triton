<?php get_header(); ?>
<div class="p404">
	<div class="p404__inner">
		<div class="p404__code">404</div>
		<div>Извините, но такой страницы нет на нашем сайте.</div>
		<div>Возможно, вы ввели неправильный адрес или страница была удалена.</div>
		<div>Попробуйте вернуться на <a href="<?php echo home_url('/'); ?>">Главную</a></div>
	</div>
</div>
<?php get_footer(); ?>