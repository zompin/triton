<?php
	global $img;

	if (!$img) {
		$img = get_post_meta(get_the_ID(), 'image', true);
	}
?>

<?php if ($img): ?>
	<div class="image">
		<img class="image__img" src="<?php echo $img; ?>">
	</div>
<?php endif; ?>