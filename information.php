<?php /*Template name: Информация*/ ?>
<?php get_header(); ?>

<?php
	$division_name = 'Информация';
	$division_url = '/category/poleznaya-informatsiya/';
?>
<?php get_template_part('inc/breadcrumbs'); ?>

<?php get_template_part('inc/image'); ?>

<main class="wrapper">

	<aside class="aside">
	<?php
		wp_nav_menu(
			array(
				'theme_location' => 'information',
				'fallback_cb' => '__return_empty_string',
				'depth' => 1,
				'container' => '',
				'menu_id' => '',
				'menu_class' => 'aside__menu'
			)
		);
	?>
	</aside>
	<div class="content">
		<?php
			the_post();
			the_content();
			$id = get_the_ID();
			$docs = get_post_meta($id, 'docs', true);
			$docs_install = get_post_meta($id, 'docs_install', true);
			$docs_bro = get_post_meta($id, 'docs_bro', true);

			if ($docs) {
				$docs_install_url = get_post_meta($id, 'docs_install_url', true);
				$docs_install_name = get_post_meta($id, 'docs_install_name', true);
				$docs_bro_url = get_post_meta($id, 'docs_bro_url', true);
				$docs_bro_name = get_post_meta($id, 'docs_bro_name', true);

				if ($docs_install_url && $docs_install_name && $docs_install) {
					show_file('ИНСТРУКЦИИ ПО МОНТАЖУ', $docs_install_url, $docs_install_name);
				}

				if ($docs_bro_url && $docs_bro_name && $docs_bro) {
					show_file('БРОШЮРЫ', $docs_bro_url, $docs_bro_name);
				}
			}

		?>
	</div>
</main>

<?php get_footer(); ?>