<div class="preview preview_product-category">
	<a href="<?php the_permalink(); ?>" class="preview__inner preview__inner_product-category" style="background-image: url(<?php echo get_post_meta($id, 'thumb', true); ?>);">
		<div class="preview__more preview__more_product-category">Подробнее</div>
		<div class="preview__title-img preview__title-img_product-category">
			<?php the_title(); ?>
		</div>
	</a>
</div>