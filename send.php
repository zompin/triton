<?php
	session_start();
	include 'phpmailer/PHPMailerAutoload.php';
	include 'mailerconfig.php';
	$mail->addAddress($to);
	$mail->addAddress($email);

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$data = $_POST;

		if (md5($data['code']) != $_SESSION['randomnr2']) {
			echo 'Вы неверно ввели каптчу';
			exit;
		}

		//exit;

		switch ($data['type']) {
			case 'calc':
				$mail->Subject = 'Заявка на детальный расчет';
				$html .= '
					<tr>
						<td colspan="2">Заявка на детальный расчет</td>
					</tr>
					<tr>
						<td>Имя: </td>
						<td>' . $data['name'] . '</td>
					</tr>
					<tr>
						<td>E-mail: </td>
						<td>' . $data['email'] . '</td>
					</tr>
					<tr>
						<td>Телефон: </td>
						<td>' . $data['phone'] . '</td>
					</tr>
					<tr>
						<td>Необходимый материал: </td>
						<td>' . $data['material'] . '</td>
					</tr>
				';
			break;
			case 'price':
				$mail->Subject = 'Заявка на прайс';
				$html .= '
					<tr>
						<td colspan="2">Заявка на прайс</td>
					</tr>
					<tr>
						<td>Имя: </td>
						<td>' . $data['name'] . '</td>
					</tr>
					<tr>
						<td>E-mail: </td>
						<td>' . $data['email'] . '</td>
					</tr>
					<tr>
						<td>Телефон: </td>
						<td>' . $data['phone'] . '</td>
					</tr>
				';
			break;
			case 'message':
				$mail->Subject = 'Сообщение';
				$html .= '
					<tr>
						<td colspan="2">Сообщение</td>
					</tr>
					<tr>
						<td>Имя: </td>
						<td>' . $data['name'] . '</td>
					</tr>
					<tr>
						<td>Телефон, E-mail: </td>
						<td>' . $data['contact'] . '</td>
					</tr>
					<tr>
						<td>Телефон: </td>
						<td>' . $data['message'] . '</td>
					</tr>
				';
			break;
			case 'dillers':
				$mail->Subject = 'Заявка диллера';
				$html .= '
					<tr>
						<td colspan="2">Заявка диллера</td>
					</tr>
					<tr>
						<td>Имя: </td>
						<td>' . $data['name'] . '</td>
					</tr>
					<tr>
						<td>Название компании: </td>
						<td>' . $data['company'] . '</td>
					</tr>
					<tr>
						<td>Телефон: </td>
						<td>' . $data['phone'] . '</td>
					</tr>
					<tr>
						<td>E-mail: </td>
						<td>' . $data['email'] . '</td>
					</tr>
					<tr>
						<td>Город: </td>
						<td>' . $data['city'] . '</td>
					</tr>
					<tr>
						<td>Сообщение: </td>
						<td>' . $data['message'] . '</td>
					</tr>
				';
			break;
			case 'qeustion':
				$mail->Subject = 'Вопрос';
				$html .= '
					<tr>
						<td colspan="2">Вопрос</td>
					</tr>
					<tr>
						<td>Имя: </td>
						<td>' . $data['name'] . '</td>
					</tr>
					<tr>
						<td>E-mail: </td>
						<td>' . $data['email'] . '</td>
					</tr>
					<tr>
						<td>Телефон: </td>
						<td>' . $data['phone'] . '</td>
					</tr>
					<tr>
						<td>Вопрос: </td>
						<td>' . $data['city'] . '</td>
					</tr>
				';
			break;
			case 'cv':
				$mail->Subject = 'Резюме';
				$html .= '
					<tr>
						<td colspan="2">Резюме</td>
					</tr>
					<tr>
						<th colspan="2">Личные данные</th>
					</tr>
					<tr>
						<td>Фамилия: </td>
						<td>' . $data['lname'] . '</td>
					</tr>
					<tr>
						<td>Имя: </td>
						<td>' . $data['fname'] . '</td>
					</tr>
					<tr>
						<td>Дата рождения: </td>
						<td>' . $data['bday'] . ' ' . $data['bmonth'] . ' ' . $data['byear'] . '</td>
					</tr>
					<tr>
						<td>Адрес: </td>
						<td>' . $data['area'] . ' ' . $data['city'] . ' ' . $data['street'] . ' ' . $data['home'] . ' ' . $data['q'] . '</td>
					</tr>
					<tr>
						<td>Телефон: </td>
						<td>' . $data['phone'] . '</td>
					</tr>
					<tr>
						<td>E-mail: </td>
						<td>' . $data['email'] . '</td>
					</tr>
					<tr>
						<td>Семейное положение: </td>
						<td>' . $data['status'] . '</td>
					</tr>
					<tr>
						<td>Дети: </td>
						<td>' . $data['kids'] . '</td>
					</tr>
					<tr>
						<th colspan="2">Образование</th>
					</tr>
					<tr>
						<td>Тип образования: </td>
						<td>' . $data['edType'] . '</td>
					</tr>
					<tr>
						<td>Учебное заведение: </td>
						<td>' . $data['univ'] . '</td>
					</tr>
					<tr>
						<td>Специальность: </td>
						<td>' . $data['spec'] . '</td>
					</tr>
					<tr>
						<th colspan="2">Опыт работы</th>
					</tr>
				';

				foreach ($data['exp'] as $exp) {
					$exp = json_decode($exp);

					$html .= '
						<tr>
							<td>Период работы: </td>
							<td>' . $exp->period . '</td>
						</tr>
						<tr>
							<td>Название компании: </td>
							<td>' . $exp->company . '</td>
						</tr>
						<tr>
							<td>Местонахождение: </td>
							<td>' . $exp->location . '</td>
						</tr>
						<tr>
							<td>Деятельность компании: </td>
							<td>' . $exp->area . '</td>
						</tr>
						<tr>
							<td>Должность: </td>
							<td>' . $exp->place . '</td>
						</tr>
					';
				}

				$html .= '
					<tr>
						<th colspan="2">Дополнительная информация</th>
					</tr>
					<tr>
						<td colspan="2">Знание языков</td>
					</tr>
					<tr>
						<td>Казахский: </td>
						<td>' . boolToWord($data['kz']) . '</td>
					</tr>
					<tr>
						<td>Русский: </td>
						<td>' . boolToWord($data['ru']) . '</td>
					</tr>
					<tr>
						<td>Английский: </td>
						<td>' . boolToWord($data['en']) . '</td>
					</tr>
					<tr>
						<td>Другой: </td>
						<td>' . otherLang($data['otherlang']) . '</td>
					</tr>
					<tr>
						<td>Водительские права: </td>
						<td>' . $data['drive'] . '</td>
					</tr>
					<tr>
						<td>Личные качества: </td>
						<td>' . $data['priv'] . '</td>
					</tr>
					<tr>
						<td>Хобби: </td>
						<td>' . $data['hob'] . '</td>
					</tr>
					<tr>
						<th colspan="2">Пожелания к будущему месту работы</th>
					</tr>
					<tr>
						<td>Область деятельности: </td>
						<td>' . $data['specArea'] . '</td>
					</tr>
					<tr>
						<td>Должность: </td>
						<td>' . $data['fplace'] . '</td>
					</tr>
					<tr>
						<td>Желаемый уровень оплаты: </td>
						<td>' . $data['pay'] . '</td>
					</tr>
				';
			break;
		}

		$mail->Body = '<table>' . $html . '</table>';

		//var_dump($_FILES);

		addFiles();

		echo $mail->send() * 1;
	}

	function boolToWord($bool) {
		if ($bool) {
			return 'Да';
		} else {
			return 'Нет';
		}
	}
	function otherLang($lang) {
		if ($lang) {
			return $lang;
		} else {
			return 'Нет';
		}
	}

	function addFiles() {
		global $mail;

		if ($_FILES && is_array($_FILES)) {
			foreach ($_FILES as $file) {
				$error = $file['error'];
				if ($error === 0) {
					$name = $file['name'];
					$tmpName = $file['tmp_name'];
					$type = $file['type'];
					$mail->addAttachment($tmpName, $name);
				}
			}
		}
	}
?>