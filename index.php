<?php get_header(); ?>

<main>
	<?php get_template_part('inc/slider'); ?>
	<section class="previews-catalog">
		<h1 class="h1 h1_mobile-hide h1_catalog">Каталог продукции</h1>
		<div class="previews-catalog__items preview-cont">
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/metallocherepitsa/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/cherepitsa.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">МЕТАЛЛОЧЕРЕПИЦА</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/proflist/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/proflist.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">ПРОФЛИСТ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/metallosaiding/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/saiding.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">МЕТАЛЛОСАЙДИНГ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/vodostok-modern/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/vod-sistemy.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">ВОДОСТОЧНЫЕ  СИСТЕМЫ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/evroshtaketnik/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/evrostak.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">Штакетник</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/profil-stroitelnyy/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/profil.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">ПРОФИЛЬ СТРОИТЕЛЬНЫЙ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/category/produktsiya/sendvich-paneli/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/sendvich-paneli.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">СЭНДВИЧ-ПАНЕЛИ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/stal-listovaya/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/stal.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">СТАЛЬ  ЛИСТОВАЯ</div>
				</a>
			</div>
			<div class="preview">
				<a href="<?php echo home_url('/shtrips-2/'); ?>" class="preview__inner preview__inner_product" style="background-image: url(<?php echo get_template_directory_uri() . '/product/shtrips.jpg'; ?>);">
					<div class="preview__link-product">ПЕРЕЙТИ В КАТАЛОГ</div>
					<div class="preview__title-img">ШТРИПС</div>
				</a>
			</div>
		</div>
	</section>
	<section class="advantages">
		<div class="advantages__items">
			<div class="advantages__item advantages__item_prod">
				<div class="advantages__item-title">
					Собственное
					<br>
					производство
				</div>
				<div class="advantages__item-desc">
					Цена завода-изготовителя всегда 
					самая выгодная цена. 
					Мы продаем свою продукцию без 
					наценок <nobr>и процентов</nobr>, сохраняя 
					самую привлекательную стоимость.
					<a href="<?php echo home_url('/servis-i-dostavka/'); ?>" class="advantages__link">
						<span></span>
						<span></span>
						<span></span>
					</a>
				</div>
			</div>
			<div class="advantages__item advantages__item_qual">
				<div class="advantages__item-title">
					Высокое качество
					<br>
					сырья
				</div>
				<div class="advantages__item-desc">
					Высокий уровень качества продукции 
					Triton гарантирован качественным 
					сырьём, закупаемым только у ведущих 
					производителей Казахстана, России 
					<nobr>и Южной Кореи.</nobr>
					<a href="<?php echo home_url('/servis-i-dostavka/'); ?>" class="advantages__link">
						<span></span>
						<span></span>
						<span></span>
					</a>
				</div>
			</div>
			<div class="advantages__item advantages__item_fast">
				<div class="advantages__item-title">
					Быстрое изготовление 
					<br>
					по вашим размерам
				</div>
				<div class="advantages__item-desc">
					Изготовим металлочерепицу
					и профлист нужного вам размера
					для уменьшения количества 
					отходов при монтаже в сроки 
					от <nobr>2 рабочих дней.</nobr>
					<a href="<?php echo home_url('/servis-i-dostavka/'); ?>" class="advantages__link">
						<span></span>
						<span></span>
						<span></span>
					</a>
				</div>
			</div>
			<div class="advantages__item advantages__item_deliv">
				<div class="advantages__item-title">
					Бережная погрузка
					<br>
					и быстрая доставка
				</div>
				<div class="advantages__item-desc">
					Погрузка готовой продукции 
					производится в открытый транспорт 
					с помощью траверс <nobr>и мягких строп. </nobr>
					Доставка выполняется ежедневно 
					<nobr>с 10:00 до 18:00 часов.</nobr>
					<a href="<?php echo home_url('/servis-i-dostavka/'); ?>" class="advantages__link">
						<span></span>
						<span></span>
						<span></span>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="previews-projects">
		<h1 class="h1">Проекты</h1>
		<div class="preview-cont previews-projects__items">
			<?php
				$posts = get_posts(array('category' => '29', 'numberposts' => 3));

				foreach ($posts as $post) {
					get_template_part('inc/preview-project');
				}
			?>
		</div>
		<div class="link-border__cont link-border__cont_projects">
			<a href="<?php echo home_url('/category/proekty/'); ?>" class="link-border link-border_projects">Перейти в галерею проектов</a>
		</div>
	</section>
	<section class="previews-news">
		<h1 class="h1">Новости</h1>
		<div class="preview-cont">
			<?php
				$posts = get_posts(array('category' => 6, 'numberposts' => 3));
				
				foreach ($posts as $post) {
					get_template_part('inc/preview-news');
				}
			?>
		</div>
		<div class="link-border__cont">
			<a href="<?php echo home_url('/category/novosti/'); ?>" class="link-border link-border_news">Перейти к новостям</a>
		</div>
	</section>
	<section class="partners">
		<h1 class="h1 h1_partners">Наши партнёры</h1>
		<div class="partners__items">
			<img src="<?php echo get_template_directory_uri() . '/partners/arcelor-mittal.png'; ?>">
			<img src="<?php echo get_template_directory_uri() . '/partners/splain.png'; ?>">
			<img src="<?php echo get_template_directory_uri() . '/partners/baostil.png'; ?>">
			<img src="<?php echo get_template_directory_uri() . '/partners/mmk.png'; ?>">
			<img src="<?php echo get_template_directory_uri() . '/partners/teh-krep.png'; ?>">
			<img src="<?php echo get_template_directory_uri() . '/partners/ursa.png'; ?>">
		</div>
		<div class="link-border__cont">
			<a href="<?php echo home_url('/dileram/'); ?>" class="link-border link-border_partner">Стать партнёром</a>
		</div>
	</section>
</main>

<?php get_footer(); ?>