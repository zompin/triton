<?php
	$id 			= $post->ID;
	$komplekt 		= get_post_meta($id, 'komplekt', true) * 1;
	$komplekt_title = get_post_meta($id, 'komplekt_title', true);
	$komplekt_image = get_post_meta($id, 'komplekt_image', true);
?>
<div class="metabox__tab">
	<input type="hidden" name="extra[komplekt]" value="">
	<input id="komplekt" type="checkbox" name="extra[komplekt]" value="1" <?php if ($komplekt) echo 'checked' ?>>
	<label for="komplekt">Комплектующие</label>
	<div class="metabox__tab-inner komplekt">
		<?php
			if ($komplekt_title && $komplekt_image) {
				foreach ($komplekt_title as $k => $v) {
					?>
						<div class="komplekt__item">
							<label class="metabox__label">
								Название комплектующего
								<textarea name="extra[komplekt_title][]"><?php echo $komplekt_title[$k]; ?></textarea>
							</label>
							<label class="metabox__label">
								Изображение комплектующего
								<textarea name="extra[komplekt_image][]"><?php echo $komplekt_image[$k]; ?></textarea>
							</label>
							<button type="button" class="button button_remove-komplekt">Удалить</button>
						</div>
					<?php
				}
			} else {
				?>
					<div class="komplekt__item">
						<label class="metabox__label">
							Название комплектующего
							<textarea name="extra[komplekt_title][]"></textarea>
						</label>
						<label class="metabox__label">
							Изображение комплектующего
							<textarea name="extra[komplekt_image][]"></textarea>
						</label>
						<button type="button" class="button button_remove-komplekt">Удалить</button>
					</div>
				<?php
			}
		?>
		<div class="button-cont">
			<button type="button" class="button button_add-komplekt">Добавить</button>
		</div>
	</div>
</div>